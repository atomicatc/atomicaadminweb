import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl:'app.component.html'
})
export class AppComponent  {
   showMenu = false;

   toggleMenu(){
     if(this.showMenu){
       this.showMenu = false;
     } else {
       this.showMenu = true;
     }
   }
 }
