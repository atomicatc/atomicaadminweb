"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const core_1 = require("@angular/core");
const platform_browser_1 = require("@angular/platform-browser");
const forms_1 = require("@angular/forms");
const http_1 = require("@angular/http");
const app_component_1 = require("./app.component");
const login_component_1 = require("./components/login/login.component");
const user_component_1 = require("./components/user.component");
const about_component_1 = require("./components/about.component");
const tabs_miembromembresia_component_1 = require("./components/tabsMiembroMembresia/tabs.miembromembresia.component");
const miembro_component_1 = require("./components/miembros/miembro.component");
const membresia_component_1 = require("./components/membresia/membresia.component");
const eventos_component_1 = require("./components/eventos/eventos.component");
const evento_info_component_1 = require("./components/eventos/secciones/evento.info.component");
const evento_equipos_component_1 = require("./components/eventos/secciones/evento.equipos.component");
const evento_rol_equipo_component_1 = require("./components/eventos/secciones/evento.rol.equipo.component");
const angular2_flash_messages_1 = require("angular2-flash-messages");
const SearchFilterPipe_1 = require("./SearchFilterPipe");
const app_routing_1 = require("./app.routing");
const app_header_component_1 = require("./components/header/app-header.component");
let AppModule = class AppModule {
};
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule, app_routing_1.routing, angular2_flash_messages_1.FlashMessagesModule],
        declarations: [app_component_1.AppComponent, SearchFilterPipe_1.SearchFilterPipe, login_component_1.LoginComponent, app_header_component_1.AppHeaderComponent,
            user_component_1.UserComponent, about_component_1.AboutComponent, tabs_miembromembresia_component_1.TabsMiembroMembresiaComponent,
            miembro_component_1.MiembroComponent, membresia_component_1.MembresiaComponent, eventos_component_1.EventosComponent, evento_info_component_1.InfoEventoComponent, evento_equipos_component_1.EquiposEventoComponent, evento_rol_equipo_component_1.RolEquipoEventoComponent],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map