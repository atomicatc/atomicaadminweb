import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { UserComponent } from './components/user.component';
import { AboutComponent } from './components/about.component';

import { TabsMiembroMembresiaComponent } from './components/tabsMiembroMembresia/tabs.miembromembresia.component';
import { MiembroComponent } from './components/miembros/miembro.component';
import { MembresiaComponent } from './components/membresia/membresia.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { InfoEventoComponent } from './components/eventos/secciones/evento.info.component';
import { EquiposEventoComponent } from './components/eventos/secciones/evento.equipos.component';
import { RolEquipoEventoComponent } from './components/eventos/secciones/evento.rol.equipo.component';

import { FlashMessagesModule } from 'angular2-flash-messages';

import { SearchFilterPipe } from './SearchFilterPipe';

import {routing} from './app.routing';
import { AppHeaderComponent } from './components/header/app-header.component';


@NgModule({
  imports: [BrowserModule, FormsModule, HttpModule, routing,FlashMessagesModule],
  declarations: [AppComponent, SearchFilterPipe, LoginComponent, AppHeaderComponent,
  UserComponent, AboutComponent,TabsMiembroMembresiaComponent,
  MiembroComponent,MembresiaComponent,EventosComponent,InfoEventoComponent,EquiposEventoComponent,RolEquipoEventoComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
