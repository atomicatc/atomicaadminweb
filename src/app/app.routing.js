"use strict";
const router_1 = require("@angular/router");
const login_component_1 = require("./components/login/login.component");
const about_component_1 = require("./components/about.component");
const miembro_component_1 = require("./components/miembros/miembro.component");
const membresia_component_1 = require("./components/membresia/membresia.component");
const eventos_component_1 = require("./components/eventos/eventos.component");
const appRoutes = [
    {
        path: '',
        component: login_component_1.LoginComponent
    },
    {
        path: 'about',
        component: about_component_1.AboutComponent
    },
    {
        path: 'miembro',
        component: miembro_component_1.MiembroComponent
    },
    {
        path: 'membresia',
        component: membresia_component_1.MembresiaComponent
    },
    {
        path: 'eventos',
        component: eventos_component_1.EventosComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map