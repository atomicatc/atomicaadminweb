import {ModuleWithProviders} from '@angular/core'; 
import {Routes, RouterModule} from '@angular/router'; 

import {LoginComponent} from './components/login/login.component';
import {UserComponent} from './components/user.component';
import {AboutComponent} from './components/about.component';

import { TabsMiembroMembresiaComponent } from './components/tabsMiembroMembresia/tabs.miembromembresia.component';
import {MiembroComponent} from './components/miembros/miembro.component';
import {MembresiaComponent} from './components/membresia/membresia.component';
import {EventosComponent} from './components/eventos/eventos.component';

const appRoutes: Routes = [
{
    path:'',
    component:LoginComponent
},
{
    path:'about',
    component:AboutComponent
},
{
    path:'miembro',
    component:MiembroComponent
},
{
    path:'membresia',
    component:MembresiaComponent
}
,
{
    path:'eventos',
    component:EventosComponent
}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);