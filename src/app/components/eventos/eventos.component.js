"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const evento_info_services_1 = require("../../services/eventos/general/evento.info.services");
const juez_services_1 = require("../../services/juez/juez.services");
const entities_1 = require("../../entities");
const angular2_flash_messages_1 = require("angular2-flash-messages");
let EventosComponent = class EventosComponent {
    constructor(eventoInfoService, juezService, flashMessagesService) {
        this.eventoInfoService = eventoInfoService;
        this.juezService = juezService;
        this.flashMessagesService = flashMessagesService;
        this.tabDatos = true;
        this.tabEquiposParticipantes = false;
        this.tabRolEquipos = false;
        this.tabRolIndividual = false;
        this.showModalNuevo = false;
        this.showModalRolEquipo = false;
        this.showModalRolIndividual = false;
        this.showAlertElimina = false;
        this.listadoEventos = new Array();
        this.mainEvento = new entities_1.Evento;
        this.cargaEventos();
    }
    ngOnInit() {
    }
    showTabDatos() {
        this.tabDatos = true;
        this.tabEquiposParticipantes = false;
        this.tabRolEquipos = false;
        this.tabRolIndividual = false;
    }
    showTabEquiposParticipantes() {
        this.tabDatos = false;
        this.tabEquiposParticipantes = true;
        this.tabRolEquipos = false;
        this.tabRolIndividual = false;
    }
    showModalRolEquipos(evento) {
        this.mainEvento = evento;
        this.showModalRolEquipo = true;
        this.tabDatos = false;
        this.tabEquiposParticipantes = false;
        this.tabRolEquipos = true;
        this.tabRolIndividual = false;
    }
    closeModalRolEquipos() {
        this.showModalRolEquipo = false;
    }
    showTabRolIndividual(evento) {
        this.mainEvento = evento;
        this.tabDatos = false;
        this.tabEquiposParticipantes = false;
        this.tabRolEquipos = false;
        this.tabRolIndividual = true;
        //this.showModalRolIndividual = true;
    }
    closeModalRolIndividual() {
        this.showModalRolIndividual = false;
    }
    toggleModal() {
        if (this.showModalNuevo) {
            this.showModalNuevo = false;
        }
        else {
            this.showModalNuevo = true;
        }
    }
    cargaEventos() {
        this.eventoInfoService.getEventos().subscribe(result => {
            if (result.error) {
                this.emptyMessage = result.error;
            }
            else {
                for (let entry of result) {
                    let evento = this.getEntryReturnEvento(entry);
                    this.listadoEventos.push(evento);
                }
            }
        }, error => {
            console.log(error);
        });
    }
    nuevoEvento() {
        this.mainEvento = new entities_1.Evento();
        this.mainEvento.fechaInicio = new Date();
        this.mainEvento.fechaTermino = new Date();
        this.mainEvento.numParticipantes = 0;
        this.mainEvento.numEquipos = 0;
        this.mainEvento.participanteXEquipo = 0;
        this.mainEvento.isEditMode = false;
        this.toggleModal();
    }
    //Metodo para obtener todos los eventos registrados ordeneados.
    //metodo para guardar la informacion del evento.
    guardaEvento() {
        console.log(this.mainEvento.jueces.length);
        this.flashMessagesService.show('Registrado...', { cssClass: 'alert-success', timeout: 1500 });
        this.mainEvento.fechaInicioNum = Date.parse(this.mainEvento.fechaInicio.toString());
        this.mainEvento.fechaTerminoNum = Date.parse(this.mainEvento.fechaTermino.toString());
        this.eventoInfoService.saveEvento(this.mainEvento).subscribe(result => {
            console.log(result);
            this.mainEvento.id = result.success;
            //ya que obtuvimos el ID se registran los jueces.
            for (let juez of this.mainEvento.jueces) {
                console.log(juez);
                juez.evento = this.mainEvento;
                this.juezService.saveJuez(juez).subscribe(result => {
                    console.log(result);
                });
            }
            this.flashMessagesService.show('Registro evento exitoso', { cssClass: 'alert-success', timeout: 1500 });
        }, err => {
            console.log(err);
            this.flashMessagesService.show('Fallo al registrar el evento: ' + err, { cssClass: 'alert-error', timeout: 1500 });
        });
    }
    loadEdit(id) {
        console.log("Edit = " + id);
        this.eventoInfoService.getEventoById(id).subscribe(result => {
            this.mainEvento = this.getEntryReturnEvento(result[0]);
            this.mainEvento.isEditMode = true;
            this.toggleModal();
        }, err => {
            console.log(err);
        });
    }
    showElimina(evento) {
        this.showAlertElimina = true;
        this.mainEvento = evento;
        console.log(this.mainEvento);
    }
    hideElimina() {
        this.showAlertElimina = false;
        this.mainEvento = new entities_1.Evento();
    }
    eliminar(id) {
        console.log("Elimina = " + id);
        this.eventoInfoService.deleteEvento(id).subscribe(result => {
            console.log(result);
            this.listadoEventos = new Array();
            this.cargaEventos();
            this.hideElimina();
            this.flashMessagesService.show('Evento eliminado correctamente.', { cssClass: 'alert-success', timeout: 1500 });
        }, err => {
            console.log(err);
        });
    }
    getEntryReturnEvento(entry) {
        let evento = new entities_1.Evento();
        evento.id = entry.idt_eventos;
        evento.nombre = entry.nombre;
        evento.slogan = entry.slogan;
        evento.numEquipos = entry.num_equipos;
        evento.numParticipantes = entry.num_participante;
        evento.participanteXEquipo = entry.part_x_equipo;
        evento.fechaInicio = new Date(entry.fecha_evento_inicia * 1);
        evento.fechaTermino = new Date(entry.fecha_evento_termina * 1);
        evento.estado = entry.estado;
        return evento;
    }
    /**
     * Metodo para actualizar los datos el evento.
     */
    updateEvento() {
        this.mainEvento.fechaInicioNum = Date.parse(this.mainEvento.fechaInicio.toString());
        this.mainEvento.fechaTerminoNum = Date.parse(this.mainEvento.fechaTermino.toString());
        this.eventoInfoService.updateEvento(this.mainEvento).subscribe(result => {
            console.log(result);
            //ya que obtuvimos el ID se registran los jueces.
            for (let juez of this.mainEvento.jueces) {
                console.log(juez);
                juez.evento = this.mainEvento;
                this.juezService.saveJuez(juez).subscribe(result => {
                    console.log(result);
                });
            }
        }, err => {
            console.log(err);
        });
    }
};
EventosComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'eventos',
        templateUrl: `eventos.component.html`,
        providers: [evento_info_services_1.EventoInfoService, juez_services_1.JuezService]
    }),
    __metadata("design:paramtypes", [evento_info_services_1.EventoInfoService,
        juez_services_1.JuezService, angular2_flash_messages_1.FlashMessagesService])
], EventosComponent);
exports.EventosComponent = EventosComponent;
//# sourceMappingURL=eventos.component.js.map