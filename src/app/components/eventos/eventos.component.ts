import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { EquiposEventoComponent } from './secciones/evento.equipos.component';

import { EventoInfoService } from '../../services/eventos/general/evento.info.services';
import { JuezService } from '../../services/juez/juez.services';

import { Evento, Juez } from '../../entities';

import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
    moduleId: module.id,
    selector: 'eventos',
    templateUrl: `eventos.component.html`,
    providers: [EventoInfoService, JuezService]
})
export class EventosComponent implements OnInit {
    tabDatos = true;
    tabEquiposParticipantes = false;
    tabRolEquipos = false;
    tabRolIndividual = false;

    showModalNuevo = false;
    showModalRolEquipo = false;
    showModalRolIndividual = false;
    showAlertElimina = false;
    listadoEventos = new Array();

    mainEvento = new Evento;

    emptyMessage: string;

    constructor(private eventoInfoService: EventoInfoService,
        private juezService: JuezService, private flashMessagesService: FlashMessagesService) {
        this.cargaEventos();
    }

    ngOnInit() {

    }

    showTabDatos() {
        this.tabDatos = true;
        this.tabEquiposParticipantes = false;
        this.tabRolEquipos = false;
        this.tabRolIndividual = false;
    }

    showTabEquiposParticipantes() {
        this.tabDatos = false;
        this.tabEquiposParticipantes = true;
        this.tabRolEquipos = false;
        this.tabRolIndividual = false;
    }

    showModalRolEquipos(evento: Evento) {
        this.mainEvento = evento;
        this.showModalRolEquipo = true;
        this.tabDatos = false;
        this.tabEquiposParticipantes = false;
        this.tabRolEquipos = true;
        this.tabRolIndividual = false;

    }

    closeModalRolEquipos(){
        this.showModalRolEquipo = false;
    }

    showTabRolIndividual(evento: Evento) {
        this.mainEvento = evento;
        this.tabDatos = false;
        this.tabEquiposParticipantes = false;
        this.tabRolEquipos = false;
        this.tabRolIndividual = true;
        //this.showModalRolIndividual = true;
    }

    closeModalRolIndividual(){
        this.showModalRolIndividual = false;
    }

    toggleModal() {
        if (this.showModalNuevo) {
            this.showModalNuevo = false;
        } else {
            this.showModalNuevo = true;
        }
    }

    cargaEventos() {
        this.eventoInfoService.getEventos().subscribe(result => {

            if (result.error) {
                this.emptyMessage = result.error;
            } else {
                for (let entry of result) {
                    let evento = this.getEntryReturnEvento(entry);

                    this.listadoEventos.push(evento);
                }
            }

        }, error => {
            console.log(error);
        });
    }

    nuevoEvento() {
        this.mainEvento = new Evento();
        this.mainEvento.fechaInicio = new Date();
        this.mainEvento.fechaTermino = new Date();
        this.mainEvento.numParticipantes = 0;
        this.mainEvento.numEquipos = 0;
        this.mainEvento.participanteXEquipo = 0;
        this.mainEvento.isEditMode = false;
        this.toggleModal();
    }


    //Metodo para obtener todos los eventos registrados ordeneados.

    //metodo para guardar la informacion del evento.
    guardaEvento() {
        console.log(this.mainEvento.jueces.length);
        this.flashMessagesService.show('Registrado...', { cssClass: 'alert-success', timeout: 1500 });
        this.mainEvento.fechaInicioNum = Date.parse(this.mainEvento.fechaInicio.toString());
        this.mainEvento.fechaTerminoNum = Date.parse(this.mainEvento.fechaTermino.toString());


        this.eventoInfoService.saveEvento(this.mainEvento).subscribe(result => {
            console.log(result);
            this.mainEvento.id = result.success;
            //ya que obtuvimos el ID se registran los jueces.
            for (let juez of this.mainEvento.jueces) {
                console.log(juez);
                juez.evento = this.mainEvento;
                this.juezService.saveJuez(juez).subscribe(result => {
                    console.log(result);
                });
            }
            this.flashMessagesService.show('Registro evento exitoso', { cssClass: 'alert-success', timeout: 1500 });
        }, err => {
            console.log(err);
            this.flashMessagesService.show('Fallo al registrar el evento: ' + err, { cssClass: 'alert-error', timeout: 1500 });
        });
    }

    loadEdit(id: number) {
        console.log("Edit = "+id);
        this.eventoInfoService.getEventoById(id).subscribe(result => {
            this.mainEvento = this.getEntryReturnEvento(result[0]);
            this.mainEvento.isEditMode = true;
            this.toggleModal();
            
        }, err => {
            console.log(err);
        });
    }

    showElimina(evento: Evento) {
        this.showAlertElimina = true;
        this.mainEvento = evento;
        console.log(this.mainEvento);
    }

    hideElimina() {
        this.showAlertElimina = false;
        this.mainEvento = new Evento();
    }

    eliminar(id: number) {
        console.log("Elimina = " + id);
        this.eventoInfoService.deleteEvento(id).subscribe(result => {
            console.log(result);
            this.listadoEventos = new Array();
            this.cargaEventos();
            this.hideElimina();
            this.flashMessagesService.show('Evento eliminado correctamente.', { cssClass: 'alert-success', timeout: 1500 });
        }, err => {
            console.log(err);
        });
    }

    getEntryReturnEvento(entry: any) {
        let evento = new Evento();

        evento.id = entry.idt_eventos;
        evento.nombre = entry.nombre;
        evento.slogan = entry.slogan;
        evento.numEquipos = entry.num_equipos;
        evento.numParticipantes = entry.num_participante;
        evento.participanteXEquipo = entry.part_x_equipo;
        evento.fechaInicio = new Date(entry.fecha_evento_inicia * 1);
        evento.fechaTermino = new Date(entry.fecha_evento_termina * 1);
        evento.estado = entry.estado;

        return evento;
    }

    /**
     * Metodo para actualizar los datos el evento.
     */
    updateEvento(){
        this.mainEvento.fechaInicioNum = Date.parse(this.mainEvento.fechaInicio.toString());
        this.mainEvento.fechaTerminoNum = Date.parse(this.mainEvento.fechaTermino.toString());

        this.eventoInfoService.updateEvento(this.mainEvento).subscribe(result => {
            console.log(result);

            //ya que obtuvimos el ID se registran los jueces.
            for (let juez of this.mainEvento.jueces) {
                console.log(juez);
                juez.evento = this.mainEvento;
                this.juezService.saveJuez(juez).subscribe(result => {
                    console.log(result);
                });
            }
        },err => {
            console.log(err);
        });
    }
    
}
