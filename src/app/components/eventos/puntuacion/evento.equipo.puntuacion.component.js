"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const evento_equiposind_services_1 = require("../../../services/eventos/general/evento.equiposind.services");
const equipo_puntuacion_services_1 = require("../../../services/eventos/puntuacion/equipo.puntuacion.services");
const evento_info_services_1 = require("../../../services/eventos/general/evento.info.services");
const entities_1 = require("../../../entities");
let EquipoPuntacionComponent = class EquipoPuntacionComponent {
    constructor(eventoEquipIndService, equipoPuntuacionService, eventoInfoService) {
        this.eventoEquipIndService = eventoEquipIndService;
        this.equipoPuntuacionService = equipoPuntuacionService;
        this.eventoInfoService = eventoInfoService;
        this.listadoEventos = new Array();
        this.listadoWods = new Array();
    }
    ngOnInit() {
        this.eventoInfoService.getEventos().subscribe(result => {
            console.log(result);
            for (let entry of result) {
                let evento = new entities_1.Evento();
                evento.id = entry.idt_eventos;
                evento.nombre = entry.nombre;
                this.listadoEventos.push(evento);
            }
        }, err => {
            console.log(err);
        });
    }
    update() {
        this.listadoWods = new Array();
        this.changeSelectedEvento(1);
    }
    changeSelectedEvento(event) {
        //console.log(this.selectedEvento);
        this.eventoEquipIndService.getEquipoByEvento(this.selectedEvento).subscribe(result => {
            //console.log(result);
            let teams = new Array();
            for (let entry of result) {
                teams.push(entry.idt_equipos);
            }
            console.log(teams.toString());
            this.equipoPuntuacionService.getWodsByTeams(teams.toString()).subscribe(result => {
                //console.log(result);
                for (let entry of result) {
                    let wod = new entities_1.WOD();
                    let order = '';
                    wod.id = entry.c_wods_idc_wods;
                    wod.nombre = entry.nombre;
                    wod.tipo = entry.tipo;
                    if (wod.tipo == 1) {
                        order = "DESC";
                    }
                    else {
                        order = "ASC";
                    }
                    this.equipoPuntuacionService.getRolByTeamsWODorder(teams.toString(), wod.id, order).subscribe(result => {
                        console.log(result);
                        for (let entry of result) {
                            let rol = new entities_1.RolEquipo();
                            let equipo = new entities_1.Equipo();
                            rol.id = entry.idt_hit_wod_equipo;
                            rol.puntuacion = entry.puntuacion;
                            rol.ranking = entry.ranking;
                            rol.resultado = entry.resultado;
                            equipo.nombre = entry.nombre_equipo;
                            rol.equipo = equipo;
                            wod.roles.push(rol);
                        }
                    }, err => {
                        console.error(err);
                    });
                    this.listadoWods.push(wod);
                }
            }, err => {
                console.error(err);
            });
        }, err => {
            console.error(err);
        });
    }
};
EquipoPuntacionComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'evento-equipo-puntuacion',
        templateUrl: `evento.equipo.puntuacion.html`,
        providers: [evento_equiposind_services_1.EventoEquipIndService, equipo_puntuacion_services_1.EquipoPuntuacionService, evento_info_services_1.EventoInfoService]
    }),
    __metadata("design:paramtypes", [evento_equiposind_services_1.EventoEquipIndService,
        equipo_puntuacion_services_1.EquipoPuntuacionService,
        evento_info_services_1.EventoInfoService])
], EquipoPuntacionComponent);
exports.EquipoPuntacionComponent = EquipoPuntacionComponent;
//# sourceMappingURL=evento.equipo.puntuacion.component.js.map