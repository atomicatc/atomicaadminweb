import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { EventoEquipIndService } from '../../../services/eventos/general/evento.equiposind.services';
import { EquipoPuntuacionService } from '../../../services/eventos/puntuacion/equipo.puntuacion.services';
import { EventoInfoService } from '../../../services/eventos/general/evento.info.services';
import { Evento, WOD, RolEquipo, Equipo } from '../../../entities';

@Component({
    moduleId: module.id,
    selector: 'evento-equipo-puntuacion',
    templateUrl: `evento.equipo.puntuacion.html`,
    providers: [EventoEquipIndService, EquipoPuntuacionService, EventoInfoService]
})
export class EquipoPuntacionComponent implements OnInit {

    listadoEventos = new Array();
    listadoWods = new Array();

    selectedEvento: number;

    constructor(private eventoEquipIndService: EventoEquipIndService,
        private equipoPuntuacionService: EquipoPuntuacionService,
        private eventoInfoService: EventoInfoService) {
    }

    ngOnInit() {
        this.eventoInfoService.getEventos().subscribe(result => {
            console.log(result);
            for (let entry of result) {
                let evento = new Evento();

                evento.id = entry.idt_eventos;
                evento.nombre = entry.nombre;

                this.listadoEventos.push(evento);
            }
        }, err => {
            console.log(err);
        });

    }

    update(){
        this.listadoWods = new Array();
        this.changeSelectedEvento(1);
    }

    changeSelectedEvento(event: any) {
        //console.log(this.selectedEvento);
        this.eventoEquipIndService.getEquipoByEvento(this.selectedEvento).subscribe(result => {
            //console.log(result);
            let teams = new Array();
            for (let entry of result) {
                teams.push(entry.idt_equipos);
            }
            console.log(teams.toString());
            this.equipoPuntuacionService.getWodsByTeams(teams.toString()).subscribe(result => {
                //console.log(result);
                for (let entry of result) {
                    let wod = new WOD();
                    let order = '';

                    wod.id = entry.c_wods_idc_wods;
                    wod.nombre = entry.nombre;
                    wod.tipo = entry.tipo;

                    if(wod.tipo==1){
                        order ="DESC";
                    } else {
                        order = "ASC";
                    }   

                    this.equipoPuntuacionService.getRolByTeamsWODorder(teams.toString(),wod.id,order).subscribe(result => {
                        console.log(result);
                        for(let entry of result){
                            let rol = new RolEquipo();
                            let equipo = new Equipo();

                            rol.id = entry.idt_hit_wod_equipo;
                            rol.puntuacion = entry.puntuacion;
                            rol.ranking = entry.ranking;
                            rol.resultado = entry.resultado;

                            equipo.nombre =entry.nombre_equipo;
                            rol.equipo = equipo;

                            wod.roles.push(rol);
                        }
                    }, err =>{
                        console.error(err);
                    });

                    this.listadoWods.push(wod);
                }
            }, err => {
                console.error(err);
            });
        }, err => {
            console.error(err);
        });
    }

}