import { Component, Input, OnInit } from '@angular/core';
import { EventoEquipIndService } from '../../../services/eventos/general/evento.equiposind.services';
import { Evento, Equipo, Participante, Categoria, Genero, TipoEquipo } from '../../../entities';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
    moduleId: module.id,
    selector: 'equipos-evento',
    templateUrl: `evento.equipos.component.html`,
    providers: [EventoEquipIndService]
})

export class EquiposEventoComponent implements OnInit {
    @Input() evento: Evento;

    equipos = new Array();
    individuales = new Array();

    categorias = new Array();
    generos = new Array();
    tipos = new Array();

    equipoAEliminar: Equipo;
    participanteAEliminar: Participante;

    showAlertEliminaEquipo = false;
    showAlertEliminaParticipante = false;

    constructor(private eventoEqIndService: EventoEquipIndService, private flashMessagesService: FlashMessagesService) {
        let categoria = new Categoria();
        categoria.id = 1;
        categoria.descripcion = "Basico";
        this.categorias.push(categoria);

        categoria = new Categoria();
        categoria.id = 2;
        categoria.descripcion = "Intermedio";
        this.categorias.push(categoria);

        categoria = new Categoria();
        categoria.id = 3;
        categoria.descripcion = "Avanzado";
        this.categorias.push(categoria);

        let genero = new Genero();
        genero.id = 1;
        genero.descripcion = "Mujer";
        this.generos.push(genero);

        genero = new Genero();
        genero.id = 2;
        genero.descripcion = "Hombre";
        this.generos.push(genero);

        let tipo = new TipoEquipo();
        tipo.id = 1;
        tipo.descripcion = "Escalado";
        this.tipos.push(tipo);

        tipo = new TipoEquipo();
        tipo.id = 2;
        tipo.descripcion = "Principiantes";
        this.tipos.push(tipo);
    }

    ngOnInit() {
        this.equipos = new Array();
        this.individuales = new Array();
        if (this.evento.isEditMode) {
            this.getEquipoByEvento(this.evento.id);
            this.getPartIndividualByEvento(this.evento.id);
        } else {
            //console.log(this.evento.equipos.length);
            //console.log(this.evento.individuales.length);

            if (this.evento.equipos.length == 0) {
                this.equipos = new Array();
                //Construye las estructuras de los equipos
                for (let i = 0; i < this.evento.numEquipos; i++) {
                    let equipo = new Equipo();
                    equipo.evento = this.evento;
                    //construyye las estructuras de los Participantes.
                    for (let i = 0; i < this.evento.participanteXEquipo; i++) {
                        let participante = new Participante();
                        participante.evento = this.evento;
                        equipo.participantes.push(participante);
                    }
                    this.equipos.push(equipo);
                }
                //console.log(this.equipos);
            } else {
                this.equipos = this.evento.equipos;
            }

            if (this.evento.individuales.length > 0) {
                this.individuales = this.evento.individuales;
            }
        }
    }

    /**
     * Metodo para agregar un nuevo registro dentro de la tabla 
     * de participacion individual.
     */
    addIndividual() {
        let individual = new Participante();
        let sinEquipo = new Equipo();
        sinEquipo.id = null;

        individual.equipo = sinEquipo;
        individual.evento = this.evento;
        individual.participaindividual = true;

        this.individuales.push(individual);
        this.flashMessagesService.show('Participante agregado', { cssClass: 'alert-success', timeout: 1000 });
    }

    /**
     * Metodo para agregar equipos al listado.
     */
    addEquipo() {
        let equipo = new Equipo();

        equipo.isNuevo = true;

        for (let i = 0; i < this.evento.participanteXEquipo; i++) {
            let participante = new Participante();
            participante.evento = this.evento;
            equipo.participantes.push(participante);
        }

        this.equipos.push(equipo);
        this.flashMessagesService.show('Equipo agregado', { cssClass: 'alert-success', timeout: 1000 });
    }

    /**
     * Metodo para salvar al equipo con sus participantes.
     * @param equipo instancia del equipo.
     */
    saveEquipo(equipo: Equipo) {
        console.log(equipo);
        equipo.evento = this.evento;
        this.eventoEqIndService.saveEquipo(equipo).subscribe(result => {
            console.log(result);
            equipo.id = result.success;
            for (let participante of equipo.participantes) {
                participante.equipo = equipo;

                this.eventoEqIndService.saveParticipante(participante).subscribe(result => {
                    console.log(result);
                    participante.id = result.success;
                });
            }
            equipo.isNuevo = false;
            this.evento.equipos.push(equipo);
            this.flashMessagesService.show('Equipo guardado exitosamente.', { cssClass: 'alert-success', timeout: 1000 });
        }, err => {
            console.log(err);
            this.flashMessagesService.show('Error al guardar el equipo.', { cssClass: 'alert-error', timeout: 1000 });
        });
        console.log(equipo);
    }

    updateEquipo(equipo: Equipo) {
        this.eventoEqIndService.updateEquipo(equipo).subscribe(result => {
            console.log(result);
            for (let participante of equipo.participantes) {
                this.eventoEqIndService.updateParticipante(participante).subscribe(result => {
                    console.log(result);
                }, err => {
                    console.log(err);
                });
            }
            this.flashMessagesService.show('Equipo actualizado exitosamente.', { cssClass: 'alert-success', timeout: 1000 });
        }, err => {
            console.log(err);
        });
    }

    saveIndividuales() {
        for (let participante of this.individuales) {
            this.eventoEqIndService.saveParticipante(participante).subscribe(result => {
                console.log(result);
                participante.id = result.success;
                this.evento.individuales.push(participante);
            }, err => {
                console.log(err);
            });
        }
        console.log(this.evento);
    }

    showDeleteMsgEquipo(equipo: Equipo) {
        this.equipoAEliminar = equipo;
        this.showAlertEliminaEquipo = true;
    }

    hideDeleteMsgEquipo() {
        this.equipoAEliminar = new Equipo();
        this.showAlertEliminaEquipo = false;
    }

    showDeleteMsgParticipante(participante: Participante) {
        this.participanteAEliminar = participante;
        this.showAlertEliminaParticipante = true;
    }

    hideDeleteMsgParticipante() {
        this.participanteAEliminar = new Participante();
        this.showAlertEliminaParticipante = false;
    }

    deleteEquipo() {
        console.log(this.equipoAEliminar);
        for (let participante of this.equipoAEliminar.participantes) {
            this.eventoEqIndService.deleteParticipante(participante.id).subscribe(result => {
                console.log(result);
            }, err => {
                console.log(err);
            });
        }
        this.eventoEqIndService.deleteEquipo(this.equipoAEliminar.id).subscribe(result => {
            console.log(result);
        }, err => {
            console.log(err);
        });
        this.equipoAEliminar = new Equipo();
        this.showAlertEliminaEquipo = false;
        this.ngOnInit();
    }

    getEquipoByEvento(id: number) {
        this.eventoEqIndService.getEquipoByEvento(id).subscribe(result => {
            //console.log(result);
            for (let entry of result) {
                //console.log(entry);
                let equipo = new Equipo();

                equipo.id = entry.idt_equipos;
                equipo.nombre = entry.nombre;
                equipo.tipokey = entry.tipo_equipo * 1;

                this.eventoEqIndService.getParticipantesByEquipo(equipo.id).subscribe(result => {

                    for (let entry of result) {

                        let participante = new Participante();

                        participante.id = entry.idt_participantes;
                        participante.nombre = entry.nombre;
                        participante.nivelkey = entry.nivel * 1;
                        participante.generokey = entry.genero * 1;
                        if (entry.add_individual == 0) {
                            participante.participaindividual = false;
                        } else {
                            participante.participaindividual = true;
                        }
                        //console.log(participante);

                        equipo.participantes.push(participante);
                    }
                }, err => {
                    console.log(err);
                });
                this.equipos.push(equipo);
            }
        }, err => {
            console.log(err);
        });
    }

    getPartIndividualByEvento(id: number) {
        this.eventoEqIndService.getParticipantesByEvento(id).subscribe(result => {
            for (let entry of result) {
                console.log(entry);
                let participante = new Participante();

                participante.id = entry.idt_participantes;
                participante.nombre = entry.nombre;
                participante.nivelkey = entry.nivel * 1;
                participante.generokey = entry.genero * 1;
                if (entry.add_individual == 0) {
                    participante.participaindividual = false;
                } else {
                    participante.participaindividual = true;
                }
                //console.log(participante);

                this.individuales.push(participante);
            }
        }, err => {
            console.log(err);
        });
    }

    saveOrUpdateindividual() {
        for (let partInd of this.individuales) {
            if (partInd.id > 0) {
                console.log("actualiza");
                this.eventoEqIndService.updateParticipante(partInd).subscribe(result => {
                    console.log(result);
                }, err => {
                    console.log(err);
                });
            } else {
                console.log("guarda");
                this.eventoEqIndService.saveParticipante(partInd).subscribe(result => {
                    console.log(result);
                    partInd.id = result.success;
                    this.evento.individuales.push(partInd);
                }, err => {
                    console.log(err);
                });
            }
        }
    }

    deleteParticipante() {
        this.eventoEqIndService.deleteParticipante(this.participanteAEliminar.id).subscribe(result => {
            console.log(result);
            this.ngOnInit();
            this.participanteAEliminar = new Participante();
            this.showAlertEliminaParticipante = false;
        }, err => {
            console.log(err);
        });
    }


}