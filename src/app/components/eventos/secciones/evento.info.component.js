"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const entities_1 = require("../../../entities");
const juez_services_1 = require("../../../services/juez/juez.services");
let InfoEventoComponent = class InfoEventoComponent {
    constructor(juezService) {
        this.juezService = juezService;
        this.juez = new entities_1.Juez;
    }
    ngOnInit() {
        console.log(this.evento.isEditMode);
        if (this.evento.isEditMode) {
            this.evento.jueces = this.getJuecesByEvento(this.evento.id);
        }
    }
    agregaJuez() {
        this.evento.jueces.push(this.juez);
        this.emptyJuecesMessage = undefined;
    }
    getJuecesByEvento(idEvento) {
        let jueces = new Array();
        this.juezService.getJuecesByIdEvento(idEvento).subscribe(result => {
            if (result.error) {
                this.emptyJuecesMessage = "Sin Jueces";
            }
            else {
                for (let entry of result) {
                    console.log(entry);
                    let juez = new entities_1.Juez();
                    juez.id = entry.idc_jueces;
                    juez.nombre = entry.nombre;
                    juez.box = entry.box;
                    jueces.push(juez);
                }
                this.emptyJuecesMessage = undefined;
            }
        }, err => {
            console.log(err);
        });
        return jueces;
    }
    /**
     * Metodo para eliminar al juez del evento.
     * @param juez Juez a Eliminar.
     */
    deleteJuez(juez) {
        if (juez.id > 0) {
            this.juezService.deleteJuez(juez.id).subscribe(result => {
                console.log(result);
                this.ngOnInit();
            }, err => {
                console.log(err);
            });
        }
        else {
            this.ngOnInit();
        }
    }
};
__decorate([
    core_1.Input(),
    __metadata("design:type", entities_1.Evento)
], InfoEventoComponent.prototype, "evento", void 0);
InfoEventoComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'info-evento',
        templateUrl: `evento.info.component.html`,
    }),
    __metadata("design:paramtypes", [juez_services_1.JuezService])
], InfoEventoComponent);
exports.InfoEventoComponent = InfoEventoComponent;
//# sourceMappingURL=evento.info.component.js.map