import { Component, Input } from '@angular/core';

import { Evento, Juez } from '../../../entities';
import { JuezService } from '../../../services/juez/juez.services';

@Component({
    moduleId: module.id,
    selector: 'info-evento',
    templateUrl: `evento.info.component.html`,
})
export class InfoEventoComponent {
    @Input() evento: Evento;

    juez = new Juez;
    emptyJuecesMessage: string

    constructor(private juezService: JuezService) {

    }

    ngOnInit() {
        console.log(this.evento.isEditMode);
        if (this.evento.isEditMode) {
            this.evento.jueces = this.getJuecesByEvento(this.evento.id);
        }
    }

    agregaJuez() {
        this.evento.jueces.push(this.juez);
        this.emptyJuecesMessage = undefined;
    }

    getJuecesByEvento(idEvento: number) {
        let jueces = new Array();
        this.juezService.getJuecesByIdEvento(idEvento).subscribe(result => {
            if (result.error) {
                this.emptyJuecesMessage = "Sin Jueces";
            } else {
                for (let entry of result) {
                    console.log(entry);
                    let juez = new Juez();

                    juez.id = entry.idc_jueces;
                    juez.nombre = entry.nombre;
                    juez.box = entry.box;

                    jueces.push(juez);
                }
                this.emptyJuecesMessage = undefined;
            }
        }, err => {
            console.log(err);
        });
        return jueces;
    }

    /**
     * Metodo para eliminar al juez del evento.
     * @param juez Juez a Eliminar.
     */
    deleteJuez(juez: Juez) {
        if (juez.id > 0) {
            this.juezService.deleteJuez(juez.id).subscribe(result => {
                console.log(result);
                this.ngOnInit();
            }, err => {
                console.log(err);
            });
        } else {
            this.ngOnInit();
        }
    }

}