"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const entities_1 = require("../../../entities");
const wods_service_1 = require("../../../services/wod/wods.service");
const evento_equiposind_services_1 = require("../../../services/eventos/general/evento.equiposind.services");
const juez_services_1 = require("../../../services/juez/juez.services");
let RolEquipoEventoComponent = class RolEquipoEventoComponent {
    constructor(wodService, eventoEqIndService, juezService) {
        this.wodService = wodService;
        this.eventoEqIndService = eventoEqIndService;
        this.juezService = juezService;
        this.numeroHits = 0;
        this.numeroWods = 0;
        this.etapa = 0;
        this.listadoWods = new Array();
        this.listadoHits = new Array();
        this.listadoEquipos = new Array();
        this.listadoJueces = new Array();
        this.listadoEtapas = new Array();
        this.catalogoWods = new Array();
    }
    ngOnInit() {
        this.getWods();
        this.getEquipoByEvento(this.evento.id);
        this.getJuecesByEvento(this.evento.id);
    }
    generaRolWods() {
        /**
         * TODO:Cambiar a un objeto de rol con el wod y el juez.
        */
        this.listadoWods = new Array();
        for (let i = 0; i < this.numeroWods; i++) {
            let config = new entities_1.ConfigWodJuez();
            let wod = new entities_1.WOD();
            let juez = new entities_1.Juez();
            this.listadoWods.push(config);
        }
    }
    generaHitslines() {
        this.listadoHits = new Array();
        for (let i = 0; i < this.numeroHits; i++) {
            let hitEquipos = new entities_1.HitEquipo();
            hitEquipos.hit = i + 1;
            for (let config of this.listadoWods) {
                let rolEquipo = new entities_1.RolEquipo();
                //TODO: Agregar al juez que se lecciones.
                rolEquipo.hit = hitEquipos.hit;
                rolEquipo.wod = config.wod;
                rolEquipo.juez = config.juez;
                rolEquipo.etapa = this.etapa;
                hitEquipos.roles.push(rolEquipo);
            }
            this.listadoHits.push(hitEquipos);
        }
    }
    guardaRolEquipos() {
        console.log(this.listadoHits);
        for (let hit of this.listadoHits) {
            for (let rol of hit.roles) {
                this.eventoEqIndService.saveRolEquipo(rol).subscribe(result => {
                    console.log(result);
                    rol.id = result.success;
                }, err => {
                    console.log(err);
                });
            }
        }
    }
    getEquipoByEvento(id) {
        this.eventoEqIndService.getEquipoByEvento(id).subscribe(result => {
            //console.log(result);
            for (let entry of result) {
                //console.log(entry);
                let equipo = new entities_1.Equipo();
                equipo.id = entry.idt_equipos;
                equipo.nombre = entry.nombre;
                equipo.tipokey = entry.tipo_equipo * 1;
                this.listadoEquipos.push(equipo);
            }
        });
    }
    getWods() {
        this.wodService.getWods().subscribe(result => {
            console.log(result);
            for (let entry of result) {
                let wod = new entities_1.WOD();
                wod.id = entry.idc_wods;
                wod.nombre = entry.nombre;
                wod.tipo = entry.tipo;
                wod.fechaRegistro = entry.fecha_registro;
                wod.duracion = entry.duracion;
                this.catalogoWods.push(wod);
            }
        }, err => {
            console.log(err);
        });
    }
    getJuecesByEvento(id) {
        this.juezService.getJuecesByIdEvento(id).subscribe(result => {
            if (result.error) {
                console.log("Sin Jueces");
            }
            else {
                for (let entry of result) {
                    console.log(entry);
                    let juez = new entities_1.Juez();
                    juez.id = entry.idc_jueces;
                    juez.nombre = entry.nombre;
                    juez.box = entry.box;
                    this.listadoJueces.push(juez);
                }
            }
        }, err => {
            console.log(err);
        });
    }
};
__decorate([
    core_1.Input(),
    __metadata("design:type", entities_1.Evento)
], RolEquipoEventoComponent.prototype, "evento", void 0);
RolEquipoEventoComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'rol-equipo-evento',
        templateUrl: `evento.rol.equipo.component.html`,
        providers: [wods_service_1.WodsService, evento_equiposind_services_1.EventoEquipIndService, juez_services_1.JuezService]
    }),
    __metadata("design:paramtypes", [wods_service_1.WodsService, evento_equiposind_services_1.EventoEquipIndService,
        juez_services_1.JuezService])
], RolEquipoEventoComponent);
exports.RolEquipoEventoComponent = RolEquipoEventoComponent;
//# sourceMappingURL=evento.rol.equipo.component.js.map