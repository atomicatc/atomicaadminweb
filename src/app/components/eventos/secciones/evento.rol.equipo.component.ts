import { Component, Input, OnInit } from '@angular/core';

import { Evento, Juez, WOD, RolEquipo, HitEquipo, ConfigWodJuez, Equipo } from '../../../entities';
import { WodsService } from '../../../services/wod/wods.service';
import { EventoEquipIndService } from '../../../services/eventos/general/evento.equiposind.services';
import { JuezService } from '../../../services/juez/juez.services';

@Component({
    moduleId: module.id,
    selector: 'rol-equipo-evento',
    templateUrl: `evento.rol.equipo.component.html`,
    providers: [WodsService, EventoEquipIndService,JuezService]
})
export class RolEquipoEventoComponent implements OnInit {
    @Input() evento: Evento;

    numeroHits = 0;
    numeroWods = 0;
    etapa = 0;

    listadoWods = new Array();
    listadoHits = new Array();
    listadoEquipos = new Array();
    listadoJueces = new Array();
    listadoEtapas = new Array();

    catalogoWods = new Array();

    constructor(private wodService: WodsService, private eventoEqIndService: EventoEquipIndService,
    private juezService: JuezService) {


    }

    ngOnInit() {
        this.getWods();
        this.getEquipoByEvento(this.evento.id);
        this.getJuecesByEvento(this.evento.id);
    }

    generaRolWods() {
        /**
         * TODO:Cambiar a un objeto de rol con el wod y el juez.
        */
        this.listadoWods = new Array();
        for (let i = 0; i < this.numeroWods; i++) {
            let config = new ConfigWodJuez();
            let wod = new WOD();
            let juez = new Juez();


            this.listadoWods.push(config);
        }
    }

    generaHitslines() {
        this.listadoHits = new Array();
        for (let i = 0; i < this.numeroHits; i++) {
            let hitEquipos = new HitEquipo();
            hitEquipos.hit = i + 1;
            for (let config of this.listadoWods) {
                let rolEquipo = new RolEquipo();
                //TODO: Agregar al juez que se lecciones.
                rolEquipo.hit = hitEquipos.hit;
                rolEquipo.wod = config.wod;
                rolEquipo.juez = config.juez;
                rolEquipo.etapa = this.etapa;

                hitEquipos.roles.push(rolEquipo);
            }
            this.listadoHits.push(hitEquipos);
        }
    }

    guardaRolEquipos() {
        console.log(this.listadoHits);
        for(let hit of this.listadoHits){
            for(let rol of hit.roles){
                this.eventoEqIndService.saveRolEquipo(rol).subscribe(result => {
                    console.log(result);
                    rol.id = result.success;
                }, err => {
                    console.log(err);
                });
            }
        }
    }

    getEquipoByEvento(id: number) {

        this.eventoEqIndService.getEquipoByEvento(id).subscribe(result => {
            //console.log(result);
            for (let entry of result) {
                //console.log(entry);
                let equipo = new Equipo();

                equipo.id = entry.idt_equipos;
                equipo.nombre = entry.nombre;
                equipo.tipokey = entry.tipo_equipo * 1;
                this.listadoEquipos.push(equipo);
            }
        });
    }

    getWods() {
        this.wodService.getWods().subscribe(result => {
            console.log(result);
            for (let entry of result) {
                let wod = new WOD();

                wod.id = entry.idc_wods;
                wod.nombre = entry.nombre;
                wod.tipo = entry.tipo;
                wod.fechaRegistro = entry.fecha_registro;
                wod.duracion = entry.duracion;

                this.catalogoWods.push(wod);
            }
        }, err => {
            console.log(err);
        });
    }

    getJuecesByEvento(id:number){
        this.juezService.getJuecesByIdEvento(id).subscribe(result => {
            if (result.error) {
               console.log("Sin Jueces");
            } else {
                for (let entry of result) {
                    console.log(entry);
                    let juez = new Juez();

                    juez.id = entry.idc_jueces;
                    juez.nombre = entry.nombre;
                    juez.box = entry.box;

                    this.listadoJueces.push(juez);
                }
            }
        }, err => {
            console.log(err);
        });
    }

}