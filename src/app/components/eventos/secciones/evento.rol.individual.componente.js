"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const entities_1 = require("../../../entities");
const wods_service_1 = require("../../../services/wod/wods.service");
const evento_equiposind_services_1 = require("../../../services/eventos/general/evento.equiposind.services");
const juez_services_1 = require("../../../services/juez/juez.services");
let RolIndividualEventoComponent = class RolIndividualEventoComponent {
    constructor(router, wodService, juezService) {
        this.router = router;
        this.wodService = wodService;
        this.juezService = juezService;
        this.numeroHits = 0;
        this.numeroWods = 0;
        this.etapa = 0;
        this.listadoWods = new Array();
        this.listadoHits = new Array();
        this.listadoJueces = new Array();
        this.catalogoWods = new Array();
    }
    ngOnInit() {
        this.getWods();
        this.getParticipantesByEvento(this.evento.id);
        this.getJuecesByEvento(this.evento.id);
    }
    generaRolWods() {
        /**
         * TODO:Cambiar a un objeto de rol con el wod y el juez.
        */
        this.listadoWods = new Array();
        for (let i = 0; i < this.numeroWods; i++) {
            let config = new entities_1.ConfigWodJuez();
            let wod = new entities_1.WOD();
            let juez = new entities_1.Juez();
            this.listadoWods.push(config);
        }
    }
    getParticipantesByEvento(eventoid) {
    }
    getWods() {
        this.wodService.getWods().subscribe(result => {
            console.log(result);
            for (let entry of result) {
                let wod = new entities_1.WOD();
                wod.id = entry.idc_wods;
                wod.nombre = entry.nombre;
                wod.tipo = entry.tipo;
                wod.fechaRegistro = entry.fecha_registro;
                wod.duracion = entry.duracion;
                this.catalogoWods.push(wod);
            }
        }, err => {
            console.log(err);
        });
    }
    getJuecesByEvento(id) {
        this.juezService.getJuecesByIdEvento(id).subscribe(result => {
            if (result.error) {
                console.log("Sin Jueces");
            }
            else {
                for (let entry of result) {
                    console.log(entry);
                    let juez = new entities_1.Juez();
                    juez.id = entry.idc_jueces;
                    juez.nombre = entry.nombre;
                    juez.box = entry.box;
                    this.listadoJueces.push(juez);
                }
            }
        }, err => {
            console.log(err);
        });
    }
};
__decorate([
    core_1.Input(),
    __metadata("design:type", entities_1.Evento)
], RolIndividualEventoComponent.prototype, "evento", void 0);
RolIndividualEventoComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'rol-individual-evento',
        templateUrl: `evento.rol.individual.component.html`,
        providers: [wods_service_1.WodsService, evento_equiposind_services_1.EventoEquipIndService, juez_services_1.JuezService]
    }),
    __metadata("design:paramtypes", [router_1.Router, wods_service_1.WodsService, juez_services_1.JuezService])
], RolIndividualEventoComponent);
exports.RolIndividualEventoComponent = RolIndividualEventoComponent;
//# sourceMappingURL=evento.rol.individual.componente.js.map