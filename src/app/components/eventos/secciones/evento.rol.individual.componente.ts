import { Component, OnInit, Input } from '@angular/core';

import { Router } from '@angular/router';
import { Evento, Juez, WOD, RolEquipo, HitEquipo, ConfigWodJuez, Equipo } from '../../../entities';
import { WodsService } from '../../../services/wod/wods.service';
import { EventoEquipIndService } from '../../../services/eventos/general/evento.equiposind.services';
import { JuezService } from '../../../services/juez/juez.services';


@Component({
    moduleId: module.id,
    selector: 'rol-individual-evento',
    templateUrl: `evento.rol.individual.component.html`,
    providers: [WodsService, EventoEquipIndService,JuezService]
})
export class RolIndividualEventoComponent implements OnInit {

     @Input() evento: Evento;

    numeroHits = 0;
    numeroWods = 0;
    etapa = 0;

    listadoWods = new Array();
    listadoHits = new Array();
    listadoJueces = new Array();
    catalogoWods = new Array();

    constructor(private router: Router,private wodService:WodsService, private juezService: JuezService){

    }

    ngOnInit() {
        this.getWods();
        this.getParticipantesByEvento(this.evento.id);
        this.getJuecesByEvento(this.evento.id);
    }

    generaRolWods() {
        /**
         * TODO:Cambiar a un objeto de rol con el wod y el juez.
        */
        this.listadoWods = new Array();
        for (let i = 0; i < this.numeroWods; i++) {
            let config = new ConfigWodJuez();
            let wod = new WOD();
            let juez = new Juez();


            this.listadoWods.push(config);
        }
    }

    getParticipantesByEvento(eventoid:number){

    }

    getWods() {
        this.wodService.getWods().subscribe(result => {
            console.log(result);
            for (let entry of result) {
                let wod = new WOD();

                wod.id = entry.idc_wods;
                wod.nombre = entry.nombre;
                wod.tipo = entry.tipo;
                wod.fechaRegistro = entry.fecha_registro;
                wod.duracion = entry.duracion;

                this.catalogoWods.push(wod);
            }
        }, err => {
            console.log(err);
        });
    }

    getJuecesByEvento(id:number){
        this.juezService.getJuecesByIdEvento(id).subscribe(result => {
            if (result.error) {
               console.log("Sin Jueces");
            } else {
                for (let entry of result) {
                    console.log(entry);
                    let juez = new Juez();

                    juez.id = entry.idc_jueces;
                    juez.nombre = entry.nombre;
                    juez.box = entry.box;

                    this.listadoJueces.push(juez);
                }
            }
        }, err => {
            console.log(err);
        });
    }

}