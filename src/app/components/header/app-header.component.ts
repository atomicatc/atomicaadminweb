import { Component } from '@angular/core';

import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'app-header',
    templateUrl: `app-header.component.html`
})
export class AppHeaderComponent {

    constructor(private router: Router){

    }

    goMiembro(){
        this.router.navigate(['miembro']);
    }

    goMembresias(){
        this.router.navigate(['membresia']);
    }

    goEventos(){
        this.router.navigate(['eventos']);
    }
}