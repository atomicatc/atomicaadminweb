"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const membresias_catalogo_service_1 = require("../../services/membresia/membresias.catalogo.service");
const periodo_catalogo_service_1 = require("../../services/periodos/periodo.catalogo.service");
const dias_asistencia_catalogo_service_1 = require("../../services/diasasistencia/dias.asistencia.catalogo.service");
const entities_1 = require("../../entities");
const angular2_flash_messages_1 = require("angular2-flash-messages");
let MembresiaComponent = class MembresiaComponent {
    constructor(membresiaService, periodoService, diasAsistenciaService, flashMessagesService) {
        this.membresiaService = membresiaService;
        this.periodoService = periodoService;
        this.diasAsistenciaService = diasAsistenciaService;
        this.flashMessagesService = flashMessagesService;
        this.membresia = new entities_1.Membresia();
        this.listado = new Array();
        this.periodos = new Array();
        this.diasAsistencias = new Array();
        this.selectedvaluePeriodo = 0;
        this.selectedvalueDias = 0;
        this.isEditMode = false;
        this.showModalDatos = false;
        this.emptyMessage = undefined;
        this.loadPeriodos();
        this.loadDias();
    }
    ngOnInit() {
        this.cargaMembresias();
        this.flashMessagesService.show('Cargando catalogo de Membresias.', { cssClass: 'alert-success', timeout: 1500 });
    }
    loadPeriodos() {
        this.periodoService.getPeriodos().subscribe(periodosResult => {
            for (let entry of periodosResult) {
                let periodo = new entities_1.Periodo();
                periodo.descripcion = entry.descripcion;
                periodo.idcPeriodo = entry.idc_periodos;
                periodo.fechaRegistro = entry.fecha_registro;
                periodo.estado = entry.estado;
                this.periodos.push(periodo);
            }
        });
    }
    showNuevaMembresia() {
        this.membresia = new entities_1.Membresia();
        this.titleDatos = "Nuevo";
        this.showModalDatos = true;
    }
    loadDias() {
        this.diasAsistenciaService.getDias().subscribe(diasResult => {
            for (let entry of diasResult) {
                let dias = new entities_1.DiasAsistencia();
                dias.idcDiasAsistencia = entry.idc_dias_asistencia;
                dias.descripcion = entry.descripcion;
                dias.fechaRegistro = entry.fecha_registro;
                dias.estado = entry.estado;
                this.diasAsistencias.push(dias);
            }
        });
    }
    cargaMembresias() {
        this.membresiaService.getMembresias().subscribe(result => {
            if (result.error) {
                this.emptyMessage = result.error;
            }
            else {
                for (let entry of result) {
                    console.log(entry); // 1, "string", false
                    let membresia = new entities_1.Membresia();
                    membresia.id = entry.idc_membresias;
                    membresia.nombreMembresia = entry.nombre;
                    membresia.costoMembresia = entry.costo;
                    membresia.fechaRegistro = entry.fecha_registro;
                    membresia.estado = entry.estado;
                    membresia.periodoId = entry.c_periodos_idc_periodos;
                    membresia.periodoDesc = entry.c_periodo_descripcion;
                    membresia.diasAsistenciaId = entry.c_dias_asisten_idc_dias_asistencia;
                    membresia.diasAsistenciaDesc = entry.c_dias_descripcion;
                    console.log(membresia);
                    this.listado.push(membresia);
                    this.emptyMessage = undefined;
                }
            }
        });
    }
    getPeriodobyId(id) {
        console.log("buscar id:  " + id);
        for (let periodo of this.periodos) {
            if (periodo.idcPeriodo == id) {
                console.log("periodo " + periodo);
                return periodo;
            }
        }
    }
    getDiasAsistenciaId(id) {
        console.log("buscar id:  " + id);
        for (let dias of this.diasAsistencias) {
            if (dias.idcDiasAsistencia == id) {
                console.log("dias: " + dias);
                return dias;
            }
        }
    }
    agregar() {
        this.membresia.periodo = this.getPeriodobyId(this.selectedvaluePeriodo);
        ;
        this.membresia.diasAsistencia = this.getDiasAsistenciaId(this.selectedvalueDias);
        console.log(this.membresia);
        this.membresiaService.saveMembresia(this.membresia).subscribe(result => {
            console.log(result);
            this.flashMessagesService.show('Se agrego la membresia ' + this.membresia.nombreMembresia, { cssClass: 'alert-success', timeout: 2500 });
            this.listado = new Array();
            this.cargaMembresias();
            this.limpiaFormulario();
        });
    }
    eliminar(id) {
        console.log("REgistro para borrar: " + id);
        this.membresiaService.deleteMembresia(id).subscribe(result => {
            console.log(result);
            this.flashMessagesService.show('Se elimino la membresia', { cssClass: 'alert-success', timeout: 2500 });
            this.listado = new Array();
            this.cargaMembresias();
        });
    }
    loadEditMembresia(membresia) {
        console.log(membresia);
        this.titleDatos = "Editar";
        this.membresia = membresia;
        this.selectedvaluePeriodo = membresia.periodoId;
        this.selectedvalueDias = membresia.diasAsistenciaId;
        this.isEditMode = true;
        this.showModalDatos = true;
    }
    updateMembresia() {
        this.membresia.periodo = this.getPeriodobyId(this.selectedvaluePeriodo);
        ;
        this.membresia.diasAsistencia = this.getDiasAsistenciaId(this.selectedvalueDias);
        console.log(this.membresia);
        this.membresiaService.updateMembresia(this.membresia).subscribe(result => {
            console.log(result);
            this.flashMessagesService.show('Se actualizo la membresia ' + this.membresia.nombreMembresia, { cssClass: 'alert-success', timeout: 2500 });
            this.listado = new Array();
            this.cargaMembresias();
            this.limpiaFormulario();
        });
    }
    limpiaFormulario() {
        this.membresia = new entities_1.Membresia();
        this.selectedvalueDias = 0;
        this.selectedvaluePeriodo = 0;
        this.isEditMode = false;
        this.showModalDatos = false;
    }
};
MembresiaComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'membresia',
        templateUrl: `membresia.component.html`,
        providers: [membresias_catalogo_service_1.MembresiaCatalogoService, periodo_catalogo_service_1.PeriodoCatalogoService, dias_asistencia_catalogo_service_1.DiasAsistenciaCatalogoService]
    }),
    __metadata("design:paramtypes", [membresias_catalogo_service_1.MembresiaCatalogoService,
        periodo_catalogo_service_1.PeriodoCatalogoService,
        dias_asistencia_catalogo_service_1.DiasAsistenciaCatalogoService,
        angular2_flash_messages_1.FlashMessagesService])
], MembresiaComponent);
exports.MembresiaComponent = MembresiaComponent;
//# sourceMappingURL=membresia.component.js.map