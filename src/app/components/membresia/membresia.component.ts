import { Component } from '@angular/core';
import { MembresiaCatalogoService } from '../../services/membresia/membresias.catalogo.service';
import { PeriodoCatalogoService } from '../../services/periodos/periodo.catalogo.service';
import { DiasAsistenciaCatalogoService } from '../../services/diasasistencia/dias.asistencia.catalogo.service';
import { Periodo, DiasAsistencia, Membresia } from '../../entities';

import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
    moduleId: module.id,
    selector: 'membresia',
    templateUrl: `membresia.component.html`,
    providers: [MembresiaCatalogoService, PeriodoCatalogoService, DiasAsistenciaCatalogoService]
})
export class MembresiaComponent {
    membresia = new Membresia();
    listado = new Array();
    periodos = new Array();
    diasAsistencias = new Array();
    emptyMessage: string;
    selectedvaluePeriodo = 0;
    selectedvalueDias = 0;
    isEditMode=false;
    showModalDatos= false;
    titleDatos:string;

    constructor(private membresiaService: MembresiaCatalogoService, 
    private periodoService: PeriodoCatalogoService, 
    private diasAsistenciaService: DiasAsistenciaCatalogoService,
    private flashMessagesService:FlashMessagesService) {
        this.emptyMessage = undefined;
        this.loadPeriodos();
        this.loadDias();
       
    }

    ngOnInit(){
        
        
         this.cargaMembresias();
        this.flashMessagesService.show('Cargando catalogo de Membresias.', { cssClass: 'alert-success', timeout: 1500 });
    }

    loadPeriodos(){
        this.periodoService.getPeriodos().subscribe(periodosResult => {
            for (let entry of periodosResult) {
                let periodo = new Periodo();

                periodo.descripcion = entry.descripcion;
                periodo.idcPeriodo = entry.idc_periodos;
                periodo.fechaRegistro = entry.fecha_registro;
                periodo.estado = entry.estado;

                this.periodos.push(periodo);
            }
        });
    }
    
    showNuevaMembresia(){
        this.membresia = new Membresia();
        this.titleDatos = "Nuevo";
        this.showModalDatos = true;
    }

    loadDias(){
        this.diasAsistenciaService.getDias().subscribe(diasResult => {
            for (let entry of diasResult) {
                let dias = new DiasAsistencia();

                dias.idcDiasAsistencia = entry.idc_dias_asistencia;
                dias.descripcion = entry.descripcion;
                dias.fechaRegistro = entry.fecha_registro;
                dias.estado = entry.estado;

                this.diasAsistencias.push(dias);
            }
        });
    }

    cargaMembresias() {
       
        this.membresiaService.getMembresias().subscribe(result => {
            
            if (result.error) {
                this.emptyMessage = result.error;
            } else {
                for (let entry of result) {
                    console.log(entry); // 1, "string", false
                    let membresia = new Membresia();

                    membresia.id = entry.idc_membresias;
                    membresia.nombreMembresia = entry.nombre;
                    membresia.costoMembresia = entry.costo;
                    membresia.fechaRegistro = entry.fecha_registro;
                    membresia.estado = entry.estado;
                    membresia.periodoId = entry.c_periodos_idc_periodos;
                    membresia.periodoDesc = entry.c_periodo_descripcion;
                    membresia.diasAsistenciaId = entry.c_dias_asisten_idc_dias_asistencia;
                    membresia.diasAsistenciaDesc = entry.c_dias_descripcion

                    console.log(membresia);
                    this.listado.push(membresia);
                    this.emptyMessage = undefined;
                }
            }
        });
    }

    getPeriodobyId(id: number) {
        console.log("buscar id:  " + id);
        for (let periodo of this.periodos) {

            if (periodo.idcPeriodo == id) {
                console.log("periodo " + periodo);
                return periodo;
            }
        }
    }

    getDiasAsistenciaId(id: number) {
        console.log("buscar id:  " + id);
        for (let dias of this.diasAsistencias) {

            if (dias.idcDiasAsistencia == id) {
                console.log("dias: " + dias);
                return dias;
            }
        }
    }

    agregar() {
        
        this.membresia.periodo = this.getPeriodobyId(this.selectedvaluePeriodo);;
        this.membresia.diasAsistencia = this.getDiasAsistenciaId(this.selectedvalueDias);

        console.log(this.membresia);
        this.membresiaService.saveMembresia(this.membresia).subscribe(result => {
            console.log(result);
            this.flashMessagesService.show('Se agrego la membresia '+this.membresia.nombreMembresia, { cssClass: 'alert-success', timeout: 2500 });
            this.listado = new Array();
            this.cargaMembresias();
            this.limpiaFormulario();
        });

    }

    eliminar(id: number) {
        console.log("REgistro para borrar: " + id);
        this.membresiaService.deleteMembresia(id).subscribe(result => {
            console.log(result);
            this.flashMessagesService.show('Se elimino la membresia', { cssClass: 'alert-success', timeout: 2500 });
            this.listado = new Array();
            this.cargaMembresias();
        });
    }

    loadEditMembresia(membresia: Membresia){
        console.log(membresia);
        
                this.titleDatos = "Editar";
                this.membresia = membresia;
                this.selectedvaluePeriodo = membresia.periodoId;
                this.selectedvalueDias = membresia.diasAsistenciaId;
                this.isEditMode = true;
                this.showModalDatos= true;
    
    }

    updateMembresia(){
        this.membresia.periodo = this.getPeriodobyId(this.selectedvaluePeriodo);;
        this.membresia.diasAsistencia = this.getDiasAsistenciaId(this.selectedvalueDias);

        console.log(this.membresia);
        this.membresiaService.updateMembresia(this.membresia).subscribe(result => {
            console.log(result);
            this.flashMessagesService.show('Se actualizo la membresia '+this.membresia.nombreMembresia, { cssClass: 'alert-success', timeout: 2500 });
            this.listado = new Array();
            this.cargaMembresias();
            this.limpiaFormulario();
            
        });
    }

    limpiaFormulario() {
        this.membresia = new Membresia();
        this.selectedvalueDias = 0;
        this.selectedvaluePeriodo = 0;
        this.isEditMode=false;
        this.showModalDatos = false;
    }
}

