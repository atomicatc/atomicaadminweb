"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const miembros_catalogo_service_1 = require("../../services/miembros/miembros.catalogo.service");
const membresias_catalogo_service_1 = require("../../services/membresia/membresias.catalogo.service");
const pagos_service_1 = require("../../services/pagos/pagos.service");
const entities_1 = require("../../entities");
const angular2_flash_messages_1 = require("angular2-flash-messages");
let MiembroComponent = class MiembroComponent {
    constructor(miembrosService, membresiaService, flashMessagesService, pagosService) {
        this.miembrosService = miembrosService;
        this.membresiaService = membresiaService;
        this.flashMessagesService = flashMessagesService;
        this.pagosService = pagosService;
        this.nuevoMiembro = new entities_1.Miembro();
        this.listado = new Array();
        this.membresiasList = new Array();
        this.selectedvalueMembresia = 0;
        this.isEditMode = false;
        this.showModal = false;
        this.listadoPagos = new Array();
        this.nuevoPago = new entities_1.Pago();
        this.showAlertEliminaMiembro = false;
        this.showAlertCancelaPago = false;
        this.showModalDatos = false;
        this.miembrosOrignal = new Array();
        this.miembrosFiltrados = new Array();
        console.log(this.listado);
        //Inicializamos las membresias que se encuentren.
        this.nuevoMiembro.fechaIngreso = new Date();
        this.cargaCatalogoMembresias();
    }
    ngOnInit() {
        this.flashMessagesService.show('Cargando catalogo de Miembros.', { cssClass: 'alert-success', timeout: 1500 });
        this.cargaMiembros();
    }
    showNuevoMiembro() {
        console.log("nuevo");
        this.nuevoMiembro = new entities_1.Miembro();
        this.nuevoMiembro.uuid = "--";
        this.nuevoMiembro.fechaIngreso = new Date();
        this.titleDatos = "Nuevo";
        this.showModalDatos = true;
        this.isEditMode = false;
    }
    cargaCatalogoMembresias() {
        this.membresiaService.getMembresias().subscribe(result => {
            for (let entry of result) {
                let membresia = new entities_1.Membresia();
                membresia.id = entry.idc_membresias;
                membresia.nombreMembresia = entry.nombre;
                membresia.costoMembresia = entry.costo;
                membresia.fechaRegistro = entry.fecha_registro;
                membresia.estado = entry.estado;
                this.membresiasList.push(membresia);
            }
        }, error => {
            console.log(error);
        });
    }
    cargaMiembros() {
        this.miembrosService.getMiembros().subscribe(result => {
            console.log(result);
            if (result.error) {
                this.emptyMessage = result.error;
            }
            else {
                for (let entry of result) {
                    let miembro = new entities_1.Miembro();
                    miembro.id = entry.idt_miembros;
                    miembro.nombre = entry.nombre_completo;
                    miembro.fechaIngreso = new Date(entry.fecha_ingreso * 1);
                    miembro.telefono = entry.telefono_principal;
                    miembro.email = entry.email;
                    miembro.membresiaId = entry.c_membresias_idc_membresias;
                    miembro.membresiaDesc = entry.c_membresia_nombre;
                    miembro.uuid = entry.uuid.toUpperCase();
                    miembro.fechaRegistro = entry.fecha_registro;
                    this.listado.push(miembro);
                }
            }
        });
    }
    getMembresiabyId(id) {
        console.log(id);
        for (let membresia of this.membresiasList) {
            if (membresia.id == id) {
                console.log(membresia);
                return membresia;
            }
        }
    }
    agregarMiembro() {
        console.log('Agregando Miembro' + this.selectedvalueMembresia);
        this.nuevoMiembro.membresia = this.getMembresiabyId(this.selectedvalueMembresia);
        this.nuevoMiembro.fechaIngresoNum = Date.parse(this.nuevoMiembro.fechaIngreso.toString());
        this.miembrosService.saveMiembros(this.nuevoMiembro).subscribe(result => {
            console.log(result);
            this.flashMessagesService.show('Se ha agregado un nuevo miembro ' + this.nuevoMiembro.nombre, { cssClass: 'alert-success', timeout: 1500 });
            this.listado = new Array();
            this.cargaMiembros();
            this.limpiarFormulario();
        });
    }
    eliminar(i) {
        console.log('Eliminando Miembro: ' + i);
        this.miembrosService.deleteMiembros(i).subscribe(result => {
            console.log(result);
            this.flashMessagesService.show('Se elimino el miembro', { cssClass: 'alert-success', timeout: 2500 });
            this.listado = new Array();
            this.showAlertEliminaMiembro = false;
            this.miembroAEliminar = new entities_1.Miembro();
            this.cargaMiembros();
        });
    }
    loadEditMiembro(miembro) {
        console.log(miembro);
        this.titleDatos = "Editar";
        this.nuevoMiembro = miembro;
        this.selectedvalueMembresia = miembro.membresiaId;
        this.isEditMode = true;
        this.showModalDatos = true;
    }
    updateMiembro() {
        this.nuevoMiembro.membresia = this.getMembresiabyId(this.selectedvalueMembresia);
        this.nuevoMiembro.fechaIngresoNum = Date.parse(this.nuevoMiembro.fechaIngreso.toString());
        this.miembrosService.updateMiembro(this.nuevoMiembro).subscribe(result => {
            console.log(result);
            this.flashMessagesService.show('Se ha actualizado el miembro ' + this.nuevoMiembro.nombre, { cssClass: 'alert-success', timeout: 1500 });
            this.listado = new Array();
            this.cargaMiembros();
            this.limpiarFormulario();
        });
    }
    showEliminaMiembro(miembro) {
        this.showAlertEliminaMiembro = true;
        this.miembroAEliminar = miembro;
    }
    hideEliminaMiembro() {
        this.showAlertEliminaMiembro = false;
        this.miembroAEliminar = new entities_1.Miembro();
    }
    limpiarFormulario() {
        this.nuevoMiembro = new entities_1.Miembro();
        this.selectedvalueMembresia = 0;
        this.isEditMode = false;
        this.filtroMiembros = undefined;
        this.showModalDatos = false;
    }
    //Metodos de pagos.
    showModalPagos(miembro) {
        this.nuevoPago = new entities_1.Pago();
        this.listadoPagos = new Array();
        this.showModal = true;
        this.emptyMessagePagos = undefined;
        console.log(miembro);
        this.getPagosOrdenadosPorMiembro(miembro.id);
        this.nuevoPago.miembro = miembro;
        let membresia = this.getMembresiabyId(miembro.membresiaId);
        this.nuevoPago.concepto = "Membresia " + membresia.nombreMembresia;
        this.nuevoPago.cantidad = membresia.costoMembresia;
    }
    hideModalPagos() {
        this.showModal = false;
    }
    getPagosOrdenadosPorMiembro(idMiembro) {
        console.log(idMiembro);
        this.pagosService.getPagosByMiembroOrdeByNewest(idMiembro).subscribe(result => {
            if (result.error) {
                this.emptyMessagePagos = result.error;
            }
            else {
                for (let entry of result) {
                    let pago = new entities_1.Pago();
                    pago.id = entry.idt_historial_pagos;
                    pago.concepto = entry.concepto;
                    pago.fechaRegistro = entry.fecha_registro;
                    pago.cantidad = entry.importe;
                    pago.estado = entry.estado;
                    this.listadoPagos.push(pago);
                }
            }
        });
    }
    registraPago() {
        this.pagosService.savePago(this.nuevoPago).subscribe(result => {
            console.log(result);
            this.flashMessagesService.show('Pago registrado por concepto' + this.nuevoPago.concepto, { cssClass: 'alert-success', timeout: 1500 });
            this.listadoPagos = new Array();
            this.getPagosOrdenadosPorMiembro(this.nuevoPago.miembro.id);
        });
    }
    cancelarPago(pago) {
        console.log(pago);
        this.pagosService.cancelPago(pago).subscribe(result => {
            console.log(result);
            this.flashMessagesService.show('Pago cancelado por concepto' + this.nuevoPago.concepto, { cssClass: 'alert-success', timeout: 1500 });
            this.listadoPagos = new Array();
            this.getPagosOrdenadosPorMiembro(this.nuevoPago.miembro.id);
        });
    }
    filtrarmiembros() {
        console.log(this.filtroMiembros);
        this.miembrosOrignal = new Array();
        this.miembrosFiltrados = new Array();
        if (this.filtroMiembros) {
            this.miembrosOrignal = this.listado;
            if (this.filtroMiembros.length > 3) {
                for (let miembro of this.listado) {
                    if (miembro.nombre.includes(this.filtroMiembros)) {
                        this.miembrosFiltrados.push(miembro);
                    }
                }
                console.log(this.miembrosFiltrados);
                this.listado = new Array();
                this.listado = this.miembrosFiltrados;
            }
            else {
                this.listado = new Array();
                this.cargaMiembros();
            }
        }
    }
};
MiembroComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'miembro',
        templateUrl: `miembro.component.html`,
        providers: [miembros_catalogo_service_1.MiembrosCatalogoService, membresias_catalogo_service_1.MembresiaCatalogoService, pagos_service_1.PagosService]
    }),
    __metadata("design:paramtypes", [miembros_catalogo_service_1.MiembrosCatalogoService,
        membresias_catalogo_service_1.MembresiaCatalogoService,
        angular2_flash_messages_1.FlashMessagesService,
        pagos_service_1.PagosService])
], MiembroComponent);
exports.MiembroComponent = MiembroComponent;
//# sourceMappingURL=miembro.component.js.map