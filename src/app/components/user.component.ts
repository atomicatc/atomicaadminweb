import { Component } from '@angular/core';
import { PostsService } from '../services/post.service';

@Component({
    moduleId: module.id,
    selector: 'user',
    templateUrl: `user.component.html`,
    providers: [PostsService]
})
export class UserComponent {
    name: string;
    email: string;
    address: address;
    hobbies: string[];
    showHobbies: boolean;
    posts: Post[];

    constructor(private postsService: PostsService) {
        console.log('Constructor ran');
        this.name = 'John Doe';
        this.email = 'email@email.com';
        this.address = {
            street: 'Azalea',
            city: 'CDMX',
            state: 'MA'
        }
        this.hobbies = ['Music', 'Movies', 'Sports']
        this.showHobbies = false;

        this.postsService.getPosts().subscribe(posts => {
            console.log(posts);
            this.posts = posts;
        });
    }

    toggleHobbies() {
        console.log("Toggle");
        if (this.showHobbies == true) {
            this.showHobbies = false;
        } else {
            this.showHobbies = true;
        }
    }

    addHobby(hobby:string) {
        console.log(hobby);
        this.hobbies.push(hobby);
    }

    deleteHobby(i:number) {
        this.hobbies.splice(i);
    }
}

interface address {
    street: string;
    city: string;
    state: string;
}

interface Post {
    id: number;
    title: string;
    body: string;
}
