"use strict";
class Membresia {
}
exports.Membresia = Membresia;
class Periodo {
}
exports.Periodo = Periodo;
class DiasAsistencia {
}
exports.DiasAsistencia = DiasAsistencia;
class Miembro {
}
exports.Miembro = Miembro;
class Pago {
}
exports.Pago = Pago;
/*
 * Entidades para el catalogo de ejercicios
 * */
/**
 *  Entidades para el catalogo de eventos
 */
class Evento {
    constructor() {
        this.jueces = new Array();
        this.equipos = new Array();
        this.individuales = new Array();
    }
}
exports.Evento = Evento;
class Juez {
}
exports.Juez = Juez;
class Equipo {
    constructor() {
        this.participantes = new Array();
        this.isNuevo = false;
    }
}
exports.Equipo = Equipo;
class Participante {
    constructor() {
        this.participaindividual = false;
    }
}
exports.Participante = Participante;
class Ejercicio {
}
exports.Ejercicio = Ejercicio;
class ConfigWodJuez {
}
exports.ConfigWodJuez = ConfigWodJuez;
class WOD {
    constructor() {
        this.roles = new Array();
    }
}
exports.WOD = WOD;
class HitEquipo {
    constructor() {
        this.roles = new Array();
    }
}
exports.HitEquipo = HitEquipo;
class RolEquipo {
}
exports.RolEquipo = RolEquipo;
class RolIndividual {
}
exports.RolIndividual = RolIndividual;
class Categoria {
}
exports.Categoria = Categoria;
class Genero {
}
exports.Genero = Genero;
class TipoEquipo {
}
exports.TipoEquipo = TipoEquipo;
//# sourceMappingURL=entities.js.map