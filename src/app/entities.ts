export class Membresia {
    id: number;
    nombreMembresia: string;
    costoMembresia: number;
    periodo: Periodo;
    periodoId:number;
    periodoDesc:string;
    diasAsistencia: DiasAsistencia;
    diasAsistenciaId: number;
    diasAsistenciaDesc:string;
    fechaRegistro: Date;
    estado: boolean;
}

export class Periodo {
    idcPeriodo: number;
    descripcion: string;
    fechaRegistro: Date;
    estado: boolean;
}

export class DiasAsistencia {
    idcDiasAsistencia: number;
    descripcion: string;
    fechaRegistro: Date;
    estado: boolean;
}

export class Miembro {
    id: number;
    nombre: string;
    fechaIngreso: Date;
    fechaIngresoNum: number;
    telefono: string;
    email: string;
    uuid: string;
    membresia: Membresia;
    membresiaId:number;
    membresiaDesc:string;
    fechaRegistro: Date;
}

export class Pago {
    id: number;
    miembro: Miembro;
    concepto: string;
    cantidad: number;
    fechaRegistro: Date;
    estado: number;
}

/*
 * Entidades para el catalogo de ejercicios 
 * */


/**
 *  Entidades para el catalogo de eventos
 */

export class Evento {
    id: number;
    nombre: string;
    slogan: string;
    fechaInicio: Date;
    fechaInicioNum: number;
    fechaTermino: Date;
    fechaTerminoNum: number;
    fechaRegistro: Date;
    estado: number;
    numParticipantes: number;
    numEquipos: number;
    participanteXEquipo: number;
    jueces = new Array();
    equipos = new Array();
    individuales = new Array();
    isEditMode:boolean;
}

export class Juez {
    id: number;
    nombre: string;
    box: string;
    fechaRegistro: Date;
    estado: number;
    evento: Evento;
}

export class Equipo {
    id: number;
    nombre: string;
    fechaRegistro: Date;
    estado: number;
    evento: Evento;
    tipokey:number;
    participantes = new Array();
    rol: [RolEquipo];
    isNuevo = false;
}

export class Participante {
    id: number;
    idAtomica: number;
    nombre: string;
    nivelkey: number;
    generokey:number;
    fechaRegistro: Date;
    participaindividual = false;
    estado: number;
    evento: Evento;
    equipo:Equipo;
    rol: [RolIndividual];
    juezAsignado: Juez;
}

export class Ejercicio {
    id: number;
    abreviatura: string;
    nombre: string;
    fechaRegistro: Date;
    estado: number;
}

export class ConfigWodJuez {
    wod:WOD;
    juez:Juez;
}

export class WOD {
    id: number;
    nombre: string;
    tipo: number;
    duracion: number;
    fechaRegistro: Date;
    estado: number;
    ejercicios: [Ejercicio];
    roles= new Array();
}

export class HitEquipo{
    hit:number;
    roles = new Array();
}

export class RolEquipo {
    id: number;
    hit: number;
    wod: WOD;
    equipo: Equipo;
    juez:Juez;
    etapa:number;
    resultado: number;
    puntuacion: number;
    ranking: number;
}

export class RolIndividual {
    id: number;
    hit: number;
    equipo: Participante;
    wod: WOD;
    resultado: number;
    puntuacion: number;
    ranking: number;
}

export class Categoria {
    id: number;
    descripcion: string;
}

export class Genero {
    id: number;
    descripcion: string;
}

export class TipoEquipo {
    id: number;
    descripcion: string;
}