import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { ConfigServices } from '../config';

@Injectable()
export class DiasAsistenciaCatalogoService {
    config = new ConfigServices();

    constructor(private http: Http) {
        console.log('DiasAsistenciaCatalogoService initialize');
    }

    getDias() {
        return this.http.get(this.config.host + '/atomicabe/catalogos/dias-asistencia.php')
            .map(res => res.json());
    }
}