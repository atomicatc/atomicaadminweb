"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const http_1 = require("@angular/http");
const config_1 = require("../../config");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
let EventoEquipIndService = class EventoEquipIndService {
    constructor(http) {
        this.http = http;
        this.config = new config_1.ConfigServices();
        console.log('PagosService initialize');
    }
    saveEquipo(equipo) {
        return this.http.post(this.config.host + '/atomicabe/evento/equipo.save.php', {
            pass_key: this.config.passkey,
            nombre: equipo.nombre,
            id_evento: equipo.evento.id,
            tipo_equipo: equipo.tipokey
        })
            .map(res => res.json());
    }
    saveParticipante(participante) {
        return this.http.post(this.config.host + '/atomicabe/evento/participante.save.php', {
            pass_key: this.config.passkey,
            nombre: participante.nombre,
            nivel: participante.nivelkey,
            genero: participante.generokey,
            id_evento: participante.evento.id,
            id_equipo: participante.equipo.id,
            add_individual: participante.participaindividual
        })
            .map(res => res.json());
    }
    getEquipoByEvento(idEvento) {
        return this.http.post(this.config.host + '/atomicabe/evento/getEquiposByEvento.php', {
            pass_key: this.config.passkey,
            id_evento: idEvento
        })
            .map(res => res.json());
    }
    getParticipantesByEquipo(idEquipo) {
        return this.http.post(this.config.host + '/atomicabe/evento/getParticipantesByEquipos.php', {
            pass_key: this.config.passkey,
            id_equipo: idEquipo
        })
            .map(res => res.json());
    }
    getParticipantesByEvento(idEvento) {
        return this.http.post(this.config.host + '/atomicabe/evento/getParticipanteByEvento.php', {
            pass_key: this.config.passkey,
            id_evento: idEvento
        })
            .map(res => res.json());
    }
    updateEquipo(equipo) {
        return this.http.post(this.config.host + '/atomicabe/evento/equipo.update.php', {
            pass_key: this.config.passkey,
            nombre: equipo.nombre,
            id_equipo: equipo.id,
            tipo_equipo: equipo.tipokey
        })
            .map(res => res.json());
    }
    updateParticipante(participante) {
        return this.http.post(this.config.host + '/atomicabe/evento/participante.update.php', {
            pass_key: this.config.passkey,
            nombre: participante.nombre,
            nivel: participante.nivelkey,
            genero: participante.generokey,
            id_participante: participante.id,
            add_individual: participante.participaindividual
        })
            .map(res => res);
    }
    deleteEquipo(idEquipo) {
        return this.http.post(this.config.host + '/atomicabe/evento/equipo.delete.php', {
            pass_key: this.config.passkey,
            id_equipo: idEquipo
        })
            .map(res => res);
    }
    deleteParticipante(idParticipante) {
        return this.http.post(this.config.host + '/atomicabe/evento/participante.delete.php', {
            pass_key: this.config.passkey,
            id_participante: idParticipante
        })
            .map(res => res);
    }
    saveRolEquipo(rolEquipo) {
        console.log(rolEquipo);
        return this.http.post(this.config.host + '/atomicabe/evento/equipo.rol.save.php', {
            pass_key: this.config.passkey,
            hit: rolEquipo.hit,
            id_wod: rolEquipo.wod.id,
            id_equipo: rolEquipo.equipo.id,
            id_juez: rolEquipo.juez.id,
            etapa: rolEquipo.etapa
        })
            .map(res => res.json());
    }
};
EventoEquipIndService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], EventoEquipIndService);
exports.EventoEquipIndService = EventoEquipIndService;
//# sourceMappingURL=evento.equiposind.services.js.map