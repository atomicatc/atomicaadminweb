import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Equipo, Participante, RolEquipo } from '../../../entities';
import { ConfigServices } from '../../config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class EventoEquipIndService {
    config = new ConfigServices();
    constructor(private http: Http) {
        console.log('PagosService initialize');
    }

    saveEquipo(equipo: Equipo) {
        return this.http.post(this.config.host + '/atomicabe/evento/equipo.save.php', {
            pass_key: this.config.passkey,
            nombre: equipo.nombre,
            id_evento: equipo.evento.id,
            tipo_equipo:equipo.tipokey
        })
            .map(res => res.json());
    }

    saveParticipante(participante: Participante) {
        return this.http.post(this.config.host + '/atomicabe/evento/participante.save.php', {
            pass_key: this.config.passkey,
            nombre: participante.nombre,
            nivel: participante.nivelkey,
            genero: participante.generokey,
            id_evento: participante.evento.id,
            id_equipo: participante.equipo.id,
            add_individual: participante.participaindividual
        })
            .map(res => res.json());
    }

    getEquipoByEvento(idEvento: number) {
        return this.http.post(this.config.host + '/atomicabe/evento/getEquiposByEvento.php', {
            pass_key: this.config.passkey,
            id_evento: idEvento
        })
            .map(res => res.json());
    }

    getParticipantesByEquipo(idEquipo:number){
        return this.http.post(this.config.host + '/atomicabe/evento/getParticipantesByEquipos.php', {
            pass_key: this.config.passkey,
            id_equipo: idEquipo
        })
            .map(res => res.json());
    }
        
    getParticipantesByEvento(idEvento:number){
        return this.http.post(this.config.host + '/atomicabe/evento/getParticipanteByEvento.php', {
            pass_key: this.config.passkey,
            id_evento: idEvento
        })
            .map(res => res.json());
    }

    updateEquipo(equipo:Equipo){
        return this.http.post(this.config.host + '/atomicabe/evento/equipo.update.php', {
            pass_key: this.config.passkey,
            nombre: equipo.nombre,
            id_equipo: equipo.id,
            tipo_equipo:equipo.tipokey
        })
            .map(res => res.json());
    }

    updateParticipante(participante: Participante) {
        return this.http.post(this.config.host + '/atomicabe/evento/participante.update.php', {
            pass_key: this.config.passkey,
            nombre: participante.nombre,
            nivel: participante.nivelkey,
            genero: participante.generokey,
            id_participante: participante.id,
            add_individual: participante.participaindividual
        })
            .map(res => res);
    }

    deleteEquipo(idEquipo:number){
        return this.http.post(this.config.host + '/atomicabe/evento/equipo.delete.php', {
            pass_key: this.config.passkey,
            id_equipo: idEquipo
        })
            .map(res => res);
    }

    deleteParticipante(idParticipante:number){
        return this.http.post(this.config.host + '/atomicabe/evento/participante.delete.php', {
            pass_key: this.config.passkey,
            id_participante: idParticipante
        })
            .map(res => res);
    }

    saveRolEquipo(rolEquipo:RolEquipo){
        console.log(rolEquipo);
        return this.http.post(this.config.host + '/atomicabe/evento/equipo.rol.save.php', {
            pass_key: this.config.passkey,
            hit: rolEquipo.hit,
            id_wod: rolEquipo.wod.id,
            id_equipo: rolEquipo.equipo.id,
            id_juez: rolEquipo.juez.id,
            etapa: rolEquipo.etapa
        })
            .map(res => res.json());
    }
}