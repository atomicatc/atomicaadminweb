"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const http_1 = require("@angular/http");
const config_1 = require("../../config");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
let EventoInfoService = class EventoInfoService {
    constructor(http) {
        this.http = http;
        this.config = new config_1.ConfigServices();
        console.log('PagosService initialize');
    }
    getEventos() {
        return this.http.get(this.config.host + '/atomicabe/evento/eventos.php')
            .map(res => res.json());
    }
    getEventoById(id) {
        return this.http.post(this.config.host + '/atomicabe/evento/evento.get.php', {
            pass_key: this.config.passkey,
            id_evento: id
        })
            .map(res => res.json());
    }
    saveEvento(evento) {
        return this.http.post(this.config.host + '/atomicabe/evento/eventos.save.php', {
            pass_key: this.config.passkey,
            nombre: evento.nombre,
            slogan: evento.slogan,
            fecha_inicia: evento.fechaInicioNum,
            fecha_termina: evento.fechaTerminoNum,
            num_participante: evento.numParticipantes,
            num_equipo: evento.numEquipos,
            part_x_equipo: evento.participanteXEquipo
        })
            .map(res => res.json());
    }
    deleteEvento(id) {
        return this.http.post(this.config.host + '/atomicabe/evento/evento.delete.php', {
            pass_key: this.config.passkey,
            id_evento: id
        })
            .map(res => res);
    }
    updateEvento(evento) {
        return this.http.post(this.config.host + '/atomicabe/evento/eventos.update.php', {
            pass_key: this.config.passkey,
            id_evento: evento.id,
            nombre: evento.nombre,
            slogan: evento.slogan,
            fecha_inicia: evento.fechaInicioNum,
            fecha_termina: evento.fechaTerminoNum,
            num_participante: evento.numParticipantes,
            num_equipo: evento.numEquipos,
            part_x_equipo: evento.participanteXEquipo
        })
            .map(res => res);
    }
};
EventoInfoService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], EventoInfoService);
exports.EventoInfoService = EventoInfoService;
//# sourceMappingURL=evento.info.services.js.map