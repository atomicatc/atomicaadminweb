import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Evento } from '../../../entities';
import { ConfigServices } from '../../config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class EventoInfoService {
    config = new ConfigServices();
    constructor(private http: Http) {
        console.log('PagosService initialize');
    }

    getEventos() {
        return this.http.get(this.config.host + '/atomicabe/evento/eventos.php')
            .map(res => res.json());
    }

    getEventoById(id: number) {
        return this.http.post(this.config.host + '/atomicabe/evento/evento.get.php', {
            pass_key: this.config.passkey,
            id_evento: id
        })
            .map(res => res.json());
    }

    saveEvento(evento: Evento) {
        return this.http.post(this.config.host + '/atomicabe/evento/eventos.save.php', {
            pass_key: this.config.passkey,
            nombre: evento.nombre,
            slogan: evento.slogan,
            fecha_inicia: evento.fechaInicioNum,
            fecha_termina: evento.fechaTerminoNum,
            num_participante: evento.numParticipantes,
            num_equipo: evento.numEquipos,
            part_x_equipo: evento.participanteXEquipo
        })
            .map(res => res.json());
    }

    deleteEvento(id: number) {
        return this.http.post(this.config.host + '/atomicabe/evento/evento.delete.php', {
            pass_key: this.config.passkey,
            id_evento: id
        })
            .map(res => res);
    }

    updateEvento(evento: Evento) {
        return this.http.post(this.config.host + '/atomicabe/evento/eventos.update.php', {
            pass_key: this.config.passkey,
            id_evento:evento.id,
            nombre: evento.nombre,
            slogan: evento.slogan,
            fecha_inicia: evento.fechaInicioNum,
            fecha_termina: evento.fechaTerminoNum,
            num_participante: evento.numParticipantes,
            num_equipo: evento.numEquipos,
            part_x_equipo: evento.participanteXEquipo
        })
            .map(res => res);
    }

}