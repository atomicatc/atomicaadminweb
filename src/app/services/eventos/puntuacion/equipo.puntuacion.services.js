"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const http_1 = require("@angular/http");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
const config_1 = require("../../config");
let EquipoPuntuacionService = class EquipoPuntuacionService {
    constructor(http) {
        this.http = http;
        this.config = new config_1.ConfigServices();
        console.log('WodsService initialize');
    }
    getWodsByTeams(equiposids) {
        return this.http.post(this.config.host + '/atomicabe/puntuacion/getWodsByEquipoEvento.php', {
            pass_key: this.config.passkey,
            ids_equipos: equiposids
        })
            .map(res => res.json());
    }
    getRolByTeamsWODorder(equiposids, wodid, ordenResultado) {
        return this.http.post(this.config.host + '/atomicabe/puntuacion/getRolEquipos.php', {
            pass_key: this.config.passkey,
            ids_equipos: equiposids,
            id_wod: wodid,
            orden: ordenResultado
        })
            .map(res => res.json());
    }
};
EquipoPuntuacionService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], EquipoPuntuacionService);
exports.EquipoPuntuacionService = EquipoPuntuacionService;
//# sourceMappingURL=equipo.puntuacion.services.js.map