import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { ConfigServices } from '../../config';

@Injectable()
export class EquipoPuntuacionService {

    config = new ConfigServices();

    constructor(private http: Http) {
        console.log('WodsService initialize');
    }

    getWodsByTeams(equiposids:string) {
        return this.http.post(this.config.host + '/atomicabe/puntuacion/getWodsByEquipoEvento.php', {
            pass_key: this.config.passkey,
            ids_equipos: equiposids
        })
            .map(res => res.json());
    }

    getRolByTeamsWODorder(equiposids:string,wodid:number,ordenResultado:string) {
        return this.http.post(this.config.host + '/atomicabe/puntuacion/getRolEquipos.php', {
            pass_key: this.config.passkey,
            ids_equipos: equiposids,
            id_wod:wodid,
            orden:ordenResultado
        })
            .map(res => res.json());
    }

}