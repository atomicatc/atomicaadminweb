"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const http_1 = require("@angular/http");
const config_1 = require("../config");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
let JuezService = class JuezService {
    constructor(http) {
        this.http = http;
        this.config = new config_1.ConfigServices();
        console.log('JuezService initialize');
    }
    getJuecesByIdEvento(idEvento) {
        return this.http.post(this.config.host + '/atomicabe/evento/getJuecesByEvento.php', {
            pass_key: this.config.passkey,
            id_evento: idEvento
        })
            .map(res => res.json());
    }
    saveJuez(juez) {
        return this.http.post(this.config.host + '/atomicabe/evento/jueces.save.php', {
            pass_key: this.config.passkey,
            nombre: juez.nombre,
            box: juez.box,
            id_evento: juez.evento.id
        })
            .map(res => res.json());
    }
    deleteJuez(idJuez) {
        return this.http.post(this.config.host + '/atomicabe/evento/jueces.delete.php', {
            pass_key: this.config.passkey,
            juez: idJuez
        })
            .map(res => res);
    }
};
JuezService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], JuezService);
exports.JuezService = JuezService;
//# sourceMappingURL=juez.services.js.map