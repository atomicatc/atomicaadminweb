import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Evento, Juez } from '../../entities';
import { ConfigServices } from '../config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class JuezService {
    config = new ConfigServices();
    constructor(private http: Http) {
        console.log('JuezService initialize');
    }

    getJuecesByIdEvento(idEvento: number) {
        return this.http.post(this.config.host + '/atomicabe/evento/getJuecesByEvento.php', {
            pass_key: this.config.passkey,
            id_evento: idEvento
        })
            .map(res => res.json());
    }

    saveJuez(juez: Juez) {
        return this.http.post(this.config.host + '/atomicabe/evento/jueces.save.php', {
            pass_key: this.config.passkey,
            nombre: juez.nombre,
            box: juez.box,
            id_evento: juez.evento.id
        })
            .map(res => res.json());
    }

    deleteJuez(idJuez:number){
        return this.http.post(this.config.host + '/atomicabe/evento/jueces.delete.php', {
            pass_key: this.config.passkey,
            juez: idJuez
        })
            .map(res => res);
    }
}