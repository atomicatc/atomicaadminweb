"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const http_1 = require("@angular/http");
const config_1 = require("../config");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
let MembresiaCatalogoService = class MembresiaCatalogoService {
    constructor(http) {
        this.http = http;
        this.config = new config_1.ConfigServices();
        console.log('MembresiaCatalogoService initialize');
    }
    getMembresias() {
        return this.http.get(this.config.host + '/atomicabe/catalogos/membresias.php')
            .map(res => res.json());
    }
    saveMembresia(membresia) {
        console.log("Save Membresia " + membresia.periodo);
        return this.http.post(this.config.host + '/atomicabe/catalogos/membresias.save.php', {
            nombreMembresia: membresia.nombreMembresia,
            costoMembresia: membresia.costoMembresia,
            idc_periodo: membresia.periodo.idcPeriodo,
            idc_dias_asistencia: membresia.diasAsistencia.idcDiasAsistencia
        })
            .map(res => res.json());
    }
    deleteMembresia(id) {
        console.log("Delete Membresia " + id);
        return this.http.post(this.config.host + '/atomicabe/catalogos/membresias.delete.php', {
            idMembresia: id
        })
            .map(res => res);
    }
    updateMembresia(membresia) {
        console.log("Save Membresia " + membresia.periodo);
        return this.http.post(this.config.host + '/atomicabe/catalogos/membresias.update.php', {
            nombreMembresia: membresia.nombreMembresia,
            costoMembresia: membresia.costoMembresia,
            idc_periodo: membresia.periodo.idcPeriodo,
            idc_dias_asistencia: membresia.diasAsistencia.idcDiasAsistencia,
            idMembresia: membresia.id
        })
            .map(res => res.json());
    }
};
MembresiaCatalogoService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], MembresiaCatalogoService);
exports.MembresiaCatalogoService = MembresiaCatalogoService;
//# sourceMappingURL=membresias.catalogo.service.js.map