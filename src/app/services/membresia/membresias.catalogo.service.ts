import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Membresia } from '../../entities'
import { ConfigServices } from '../config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class MembresiaCatalogoService {
    config = new ConfigServices();

    constructor(private http: Http) {
        console.log('MembresiaCatalogoService initialize');
    }

    getMembresias() {
        return this.http.get(this.config.host + '/atomicabe/catalogos/membresias.php')
            .map(res => res.json());
    }

    saveMembresia(membresia: Membresia) {
        console.log("Save Membresia " + membresia.periodo);
        return this.http.post(this.config.host + '/atomicabe/catalogos/membresias.save.php',
            {
                nombreMembresia: membresia.nombreMembresia,
                costoMembresia: membresia.costoMembresia,
                idc_periodo: membresia.periodo.idcPeriodo,
                idc_dias_asistencia: membresia.diasAsistencia.idcDiasAsistencia
            })
            .map(res => res.json());
    }

    deleteMembresia(id: number) {
        console.log("Delete Membresia " + id);
        return this.http.post(this.config.host + '/atomicabe/catalogos/membresias.delete.php',
            {
                idMembresia: id
            })
            .map(res => res);
    }

    updateMembresia(membresia: Membresia) {
        console.log("Save Membresia " + membresia.periodo);
        return this.http.post(this.config.host + '/atomicabe/catalogos/membresias.update.php',
            {
                nombreMembresia: membresia.nombreMembresia,
                costoMembresia: membresia.costoMembresia,
                idc_periodo: membresia.periodo.idcPeriodo,
                idc_dias_asistencia: membresia.diasAsistencia.idcDiasAsistencia,
                idMembresia: membresia.id
            })
            .map(res => res.json());
    }
}