"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const http_1 = require("@angular/http");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
const config_1 = require("../config");
let MiembrosCatalogoService = class MiembrosCatalogoService {
    constructor(http) {
        this.http = http;
        this.config = new config_1.ConfigServices();
        console.log('MiembrosCatalogoService initialize');
    }
    getMiembros() {
        return this.http.get(this.config.host + '/atomicabe/catalogos/miembros.php')
            .map(res => res.json());
    }
    saveMiembros(miembro) {
        console.log("SaveMiembro -> " + miembro);
        return this.http.post(this.config.host + '/atomicabe/catalogos/miembros.save.php', {
            nombre: miembro.nombre,
            id_membresia: miembro.membresia.id,
            fecha_ingreso: miembro.fechaIngresoNum,
            numero_telefonico: miembro.telefono,
            correo_electronico: miembro.email,
            uuid: miembro.uuid
        })
            .map(res => res.json());
    }
    deleteMiembros(id) {
        console.log("Delete miembro " + id);
        return this.http.post(this.config.host + '/atomicabe/catalogos/miembros.delete.php', {
            idMiembro: id
        })
            .map(res => res);
    }
    updateMiembro(miembro) {
        console.log(miembro);
        return this.http.post(this.config.host + '/atomicabe/catalogos/miembros.update.php', {
            id_miembro: miembro.id,
            nombre: miembro.nombre,
            id_membresia: miembro.membresia.id,
            fecha_ingreso: miembro.fechaIngresoNum,
            numero_telefonico: miembro.telefono,
            correo_electronico: miembro.email,
            uuid: miembro.uuid,
        })
            .map(res => res.json());
    }
};
MiembrosCatalogoService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], MiembrosCatalogoService);
exports.MiembrosCatalogoService = MiembrosCatalogoService;
//# sourceMappingURL=miembros.catalogo.service.js.map