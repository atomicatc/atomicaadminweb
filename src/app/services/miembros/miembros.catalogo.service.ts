import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Miembro } from '../../entities'
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { ConfigServices } from '../config';

@Injectable()
export class MiembrosCatalogoService {
    config = new ConfigServices();

    constructor(private http: Http) {
        console.log('MiembrosCatalogoService initialize');
    }

    getMiembros() {
        return this.http.get(this.config.host + '/atomicabe/catalogos/miembros.php')
            .map(res => res.json());
    }

    saveMiembros(miembro:Miembro) {
        console.log("SaveMiembro -> " + miembro);
        return this.http.post(this.config.host + '/atomicabe/catalogos/miembros.save.php',
         {
                nombre: miembro.nombre,
                id_membresia: miembro.membresia.id,
                fecha_ingreso: miembro.fechaIngresoNum,
                numero_telefonico: miembro.telefono,
                correo_electronico: miembro.email,
                uuid: miembro.uuid
            })
            .map(res => res.json());
    }


    deleteMiembros(id: number) {
        console.log("Delete miembro " + id);
        return this.http.post(this.config.host + '/atomicabe/catalogos/miembros.delete.php',
            {
                idMiembro: id
            })
            .map(res => res);
    }

    updateMiembro(miembro:Miembro){
        console.log(miembro);
        return this.http.post(this.config.host + '/atomicabe/catalogos/miembros.update.php',
         {
                id_miembro:miembro.id,
                nombre: miembro.nombre,
                id_membresia: miembro.membresia.id,
                fecha_ingreso: miembro.fechaIngresoNum,
                numero_telefonico: miembro.telefono,
                correo_electronico: miembro.email,
                uuid: miembro.uuid,
            })
            .map(res => res.json());
    }
}