import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Pago } from '../../entities';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { ConfigServices } from '../config';

@Injectable()
export class PagosService {
    config = new ConfigServices();

    constructor(private http: Http) {
        console.log('PagosService initialize');
    }

    getPagosByMiembroOrdeByNewest(idMiembro: number) {
        console.log(idMiembro);
        return this.http.post(this.config.host + '/atomicabe/catalogos/pagos.php',{
                id_miembro: idMiembro
            })
            .map(res => res.json());
    }

    savePago(pago: Pago){
        return this.http.post(this.config.host + '/atomicabe/catalogos/pagos.save.php',{
                id_miembro: pago.miembro.id,
                concepto:pago.concepto,
                importe: pago.cantidad,
            })
            .map(res => res.json());
    }

    cancelPago(pago: Pago){
        console.log(pago.id);
        return this.http.post(this.config.host + '/atomicabe/catalogos/pagos.cancel.php',{
                id_pago: pago.id
            })
            .map(res => res.json());
    }
}