import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { ConfigServices } from '../config';

@Injectable()
export class PeriodoCatalogoService {

    config = new ConfigServices();

    constructor(private http: Http) {
        console.log('PeriodoCatalogoService initialize');
    }

    getPeriodos() {
        return this.http.get(this.config.host + '/atomicabe/catalogos/periodos.php')
            .map(res => res.json());
    }
}