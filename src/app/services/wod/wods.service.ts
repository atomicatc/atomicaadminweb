import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { ConfigServices } from '../config';

@Injectable()
export class WodsService {

    config = new ConfigServices();

    constructor(private http: Http) {
        console.log('WodsService initialize');
    }

    getWods() {
        return this.http.post(this.config.host + '/atomicabe/catalogos/wods.php', {
            pass_key: this.config.passkey
        })
            .map(res => res.json());
    }

}